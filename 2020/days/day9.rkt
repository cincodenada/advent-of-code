#lang racket
(require "common.rkt")
(require racket/generator)
(require "day1.rkt")

(provide find-imposter find-sum-sequence)
(provide solve)

;(apply vector (map string->number (take (read-lines (input 9)) 25)))

(define (ring-buffer source vec)
  (generator ()
             (let ([size (vector-length vec)])
               (define (ring-next vec pos source)
                 (match source
                   [(cons val rem)
                    (yield val)
                    (vector-set! vec pos val)
                    (ring-next vec (modulo (+ 1 pos) size) rem)]))
               (ring-next vec 0 source))))
(define (ring-input)
  (ring-buffer (map string->number (read-lines (input 9))) (make-vector 25)))

(define (find-imposter input veclen)
  (let ([ring-vec (make-vector veclen)])
    (define (find-imposter ring)
      (let ([suspect (ring)])
        ; This vector->list is hardly efficient, but whatevs
        (if (and (not (= 0 (vector-ref ring-vec (- veclen 1))))
                 (empty? (find-sum-friends suspect 2 (vector->list ring-vec))))
            suspect
            (find-imposter ring))))
    (find-imposter (ring-buffer input ring-vec))))

(define (find-sum-sequence numbers target)
  (define (find-sum-sequence numbers target candidates sum)
    (display-line sum)
    (cond
     [(= sum target) (list (apply min candidates) (apply max candidates))]
     [(> sum target) (find-sum-sequence numbers target (rest candidates) (- sum (first candidates)))]
     [(< sum target) (find-sum-sequence (mrest numbers) target (append candidates (list (mfirst numbers))) (+ sum (mfirst numbers)))]))
  (find-sum-sequence numbers target '() 0))

(define (solve part)
  (match part
    [1 (find-imposter (map string->number (read-lines (input 9))) 25)]
    [2 (apply + (find-sum-sequence (stream-map string->number (read-stream (input 9))) (solve 1)))]))