#lang racket
(require racket/pretty)
(require "common.rkt")

(provide find-sum-friends)
(provide solve solve-cmd)

(define (find-sum-friends target count candidates)
  (if (empty? candidates)
      empty
      (let ([cur (first candidates)]
            [others (rest candidates)])
        (if (= count 1)
            (if (= target cur)
                (list cur)
                (find-sum-friends target count others))
            (let ([friends (find-sum-friends (- target cur) (- count 1) others)])
              (if (cons? friends)
                  (cons cur friends)
                  (find-sum-friends target count others)))))))

(define (solve target count)
  (let ([answers (find-sum-friends target count (map string->number (read-lines (input 1))))])
    (pretty-print answers)
    (apply * answers)))

(define (solve-cmd part target)
  (solve
   (string->number target)
   (+ 1 (string->number part))))
