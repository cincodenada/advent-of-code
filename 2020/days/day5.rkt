#lang racket
(require "common.rkt")

(provide reduce-bounds find-seat seat-id find-gap)
(provide solve)

(define (split lo hi break-direction)
  (+ lo
     (quotient (- hi lo) 2)
     (match break-direction ['lo 0] ['hi 1])))

(define (reduce-bounds reducers f b l r)
  (match reducers
    ['() (values f b l r)]
    [(cons #\F rem) (reduce-bounds rem f (split f b 'lo) l r)]
    [(cons #\B rem) (reduce-bounds rem (split f b 'hi) b l r)]
    [(cons #\L rem) (reduce-bounds rem f b l (split l r 'lo))]
    [(cons #\R rem) (reduce-bounds rem f b (split l r 'hi) r)]))

(define (find-seat locator)
  (match/values (reduce-bounds (string->list locator) 0 127 0 7)
    [(f b l r)
     #:when (and (= f b) (= l r))
     (list f l)]
    [(f b l r) (error "Locator not fully specified, bounds:" f b l r)]))

(define (seat-id locator)
  (match (find-seat locator)
    [(list row col)
     (+ (* row 8) col)]))

(define (find-gap idlist)
  (match (sort idlist <)
    ['()
  (let ([idlist (sort idlist <)])

    (if (= 2 (apply - (second idlist) (first idlist)))
        (+ 1 (first idlist))
        (find-gap (rest idlist)))))

(define (solve part)
  (match part
    [1 (apply max (map seat-id (read-lines (input 5))))]
    [2 (find-gap (map seat-id (read-lines (input 5))))]))
