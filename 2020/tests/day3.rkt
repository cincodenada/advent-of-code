#lang racket
(require test-engine/racket-tests)
(require "../days/day3.rkt")

(define (small-forest) (string-split
 "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#"
 "\n"))

(define (forest-map) (make-a-hash-of-it (small-forest)))

(check-expect (find-trees (first (small-forest))) '(2 3))
(check-expect (find-trees "......") empty)
(check-expect (find-trees "######") '(0 1 2 3 4 5))

(check-expect (hash-has-key? (get-field trees (forest-map)) '(0 3)) #t)
(check-expect (collide-trees (forest-map) '(0 0) '(1 3))
              '(
                (2 6)
                (4 12)
                (5 15)
                (7 21)
                (8 24)
                (9 27)
                (10 30)))
                                                                    
(check-expect (solve 1) 169)
(check-expect (solve 2) 7560370818)

(test)