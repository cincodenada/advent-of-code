mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day19;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;

use std::fs;
use std::time::Instant;

pub fn run(day: Option<u8>, part: Option<u8>) {
  let day = day.unwrap_or(2);

  let time = Instant::now();
  let input = get_input(day);
  println!("Read input in {}ms", time.elapsed().as_micros() as f32 / 1000.0);

  let start = Instant::now();
  let answer = run_day(day, part, input);
  let time = start.elapsed().as_micros();

  println!("{}", answer);
  println!("Ran in {}ms", time as f32 / 1000.0);
}

fn run_day(day: u8, part: Option<u8>, input: String) -> String {
  match day {
    1 => day1::run(input, part),
    2 => day2::run(input, part),
    3 => day3::run(input, part),
    4 => day4::run(input, part),
    5 => day5::run(input, part),
    6 => day6::run(input, part),
    7 => day7::run(input, part),
    8 => day8::run(input, part),
    9 => day9::run(input, part),
    10 => day10::run(input, part),
    11 => day11::run(input, part),
    12 => day12::run(input, part),
    13 => day13::run(input, part),
    14 => day14::run(input, part),
    15 => day15::run(input, part),
    16 => day16::run(input, part),
    17 => day17::run(input, part),
    19 => day19::run(input, part),
    20 => day20::run(input, part),
    21 => day21::run(input, part),
    22 => day22::run(input, part),
    23 => day23::run(input, part),
    24 => day24::run(input, part),
    25 => day25::run(input, part),
    n => panic!("Day {} not found!", n)
  }
}

pub fn get_input(day : u8) -> String {
  match fs::read_to_string(format!("input/day{}.dat", day)) {
    Err(why) => panic!("Couldn't find data file: {}", why),
    Ok(content) => content
  }
}

#[cfg(test)]
mod answers {
  use super::*;

  fn check_day(day: u8, ans1: &str, ans2: &str) {
    let input = get_input(day);
    assert_eq!(ans1, run_day(day, Some(1), input.clone()));
    assert_eq!(ans2, run_day(day, Some(2), input));
  }

  #[test] fn day1() { check_day(1, "3465245", "5194970"); }
  #[test] fn day2() { check_day(2, "6730673", "3749"); }
  #[test] fn day3() { check_day(3, "860", "9238"); }
  #[test] fn day4() { check_day(4, "1748", "1180"); }
  #[test] fn day5() { check_day(5, "10987514", "14195011"); }
  #[test] fn day6() { check_day(6, "142915", "283"); }
  #[test] fn day7() { check_day(7, "929800", "15432220"); }
  #[test]
  fn day8() {
    check_day(8, "2520",concat!(
    "█    ████  ██    ██ █   █\n",
    "█    █    █  █    █ █   █\n",
    "█    ███  █       █  █ █ \n",
    "█    █    █ ██    █   █  \n",
    "█    █    █  █ █  █   █  \n",
    "████ ████  ███  ██    █  "));
  }
  #[test] fn day9() { check_day(9, "2518058886", "44292"); }
  #[test] fn day10() { check_day(10, "347", "829"); }
  #[test]
  fn day11() {
    check_day(11, "2172", concat!(
"                                             \n",
"    ██ ████ █    ████ ████  ██  █  █ ███     \n",
"     █ █    █    █    █    █  █ █  █ █  █    \n",
"     █ ███  █    ███  ███  █    ████ █  █    \n",
"     █ █    █    █    █    █ ██ █  █ ███     \n",
"  █  █ █    █    █    █    █  █ █  █ █       \n",
"   ██  ████ ████ ████ █     ███ █  █ █       \n",
"                                             \n"));
  }
  #[test] fn day12() { check_day(12, "12053", "320380285873116"); }
  #[ignore] #[test] fn day13() { check_day(13, "200", "9803"); }
  #[test] fn day14() { check_day(14, "374457", "3568888"); }
  #[ignore] #[test] fn day15() { check_day(15, "242", "276"); }
  #[ignore] #[test] fn day16() { check_day(16, "74369033", ""); }
  #[test] fn day17() { check_day(17, "5068", ""); }
  #[test] fn day19() { check_day(19, "112", "18261982"); }
  #[ignore] #[test] fn day20() { check_day(20, "690", "7976"); }
  #[test] fn day21() { check_day(21, "19352864", "1142488337"); }
  #[test] fn day22() { check_day(22, "4086", ""); }
  #[test] fn day23() { check_day(23, "19040", "11041"); }
  #[test] fn day24() { check_day(24, "18400821", "1914"); }
}
