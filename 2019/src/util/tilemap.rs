use super::geom::Vec2;
use std::cmp;
use std::collections::HashMap;

#[derive(Clone)]
pub struct TileMap<T> {
  map: HashMap<Vec2, T>,
  overlay: HashMap<Vec2, char>,
  pub axes: bool,
}
impl<T> TileMap<T>
where T: PartialEq {
  pub fn new() -> TileMap<T> {
    TileMap { 
      map: HashMap::new(),
      overlay: HashMap::new(),
      axes: true,
    }
  }

  pub fn keys(&self) -> impl Iterator + '_ { self.map.keys() }
  pub fn iter(&self) -> impl Iterator<Item = (&Vec2, &T)> + '_ {
    self.map.iter()
  }
  pub fn set(&mut self, p: Vec2, t: T) {
    self.map.insert(p, t); 
  }
  pub fn set_xy(&mut self, x: i32, y: i32, t: T) {
    self.set(Vec2::from_xy(x, y), t);
  }
  pub fn get(&self, p: &Vec2) -> Option<&T> {
    self.map.get(p)
  }
  pub fn get_xy(&self, x: i32, y: i32) -> Option<&T> {
    self.get(&Vec2::from_xy(x, y))
  }

  pub fn count(&self, val: T) -> usize {
    self.map.values().filter(|&v| *v == val).count()
  }

  pub fn add_overlay(&mut self, p: Vec2, c: char) {
    self.overlay.insert(p, c);
  }
  
  pub fn set_overlay(&mut self, overlays: Vec<(Vec2, char)>) {
    self.overlay = HashMap::new();
    for (p, c) in overlays {
      self.overlay.insert(p, c);
    }
  }

  pub fn range(&self) -> (Vec2, Vec2) {
    // TODO: Pre-calculate this on insert
    if self.map.keys().len() == 0 { return (Vec2::new(), Vec2::new()) }
    let mut max = Vec2::from_xy(std::i32::MIN, std::i32::MIN);
    let mut min = Vec2::from_xy(std::i32::MAX, std::i32::MAX);
    for p in self.map.keys() {
      max.x = cmp::max(max.x, p.x);
      min.x = cmp::min(min.x, p.x);
      max.y = cmp::max(max.y, p.y);
      min.y = cmp::min(min.y, p.y);
    }
    (min, max)
  }

  pub fn draw<F>(&self, charsel: F) -> String
    where F: Fn(Option<&T>) -> char
  {
    self.draw_impl(charsel, false)
  }
  pub fn draw_impl<F>(&self, charsel: F, axes: bool) -> String
    where F: Fn(Option<&T>) -> char
  {
    let (min, max) = self.range();

    let mut out = String::new();
    for y in min.y..=max.y {
      for x in min.x..=max.x {
        let loc = Vec2::from_xy(x, y);
        out.push(match self.overlay.get(&loc) {
          Some(&c) => c,
          None => charsel(self.map.get(&loc))
        });
      }
      if axes {
        out.push('|');
        out.push_str(&y.to_string());
      }
      out.push('\n');
    }
    if axes {
      let xwidth = (max.x-min.x+1) as usize;
      out.push_str(&"-".repeat(xwidth));
      out.push('\n');
      out.push_str(&format!("{:<wl$}{:>wr$}", min.x, max.x, wl=xwidth/2, wr=(xwidth+1)/2));
    }
    out
  }
}
impl TileMap<char> {
  pub fn from_str(chars: &str) -> TileMap<char> {
    let mut map = TileMap::new();
    for (y, l) in chars.lines().enumerate() {
      for (x, c) in l.chars().enumerate() {
        map.set_xy(x as i32, y as i32, c);
      }
    }
    map
  }

  pub fn drawc(&self) -> String { self.draw(|c| *c.unwrap_or(&' ')) }
}
