use std::collections::{VecDeque, HashMap};
use std::io;
use std::io::prelude::*;

const MAX_CYCLES: u64 = 100000000;
const MAX_VARS: usize = 5;

pub type Value = i128;

#[derive(PartialEq, Debug)]
pub enum ProgState {
  STOPPED,
  RUNNING,
  READ,
  WRITE,
  DONE,
  ABORTED
}

pub struct Program {
  pc: usize,
  rel_base: Value,
  cycles: u64,
  ints: Vec<Value>,
  prog: Vec<Value>,
  modes: Vec<u8>,
  pub state: ProgState,
  // TODO: These should be VecDeque too
  // Also, remove direct access
  pub input: Vec<Value>,
  pub output: Vec<Value>,
  recent_vars: VecDeque<(usize, Value)>,
  pub synchronous: bool,
}

impl Program {
  pub fn from_vec(prog: Vec<Value>) -> Program {
    Program {
      ints: prog.clone(),
      prog,
      pc: 0,
      rel_base: 0,
      cycles: 0,
      state: ProgState::STOPPED,
      modes: Vec::new(),
      input: Vec::new(),
      output: Vec::new(),
      recent_vars: VecDeque::new(),
      synchronous: false,
    }
  }
  pub fn from_vec_input(prog: Vec<Value>, input: Vec<Value>) -> Program {
    Program {
      input,
      ..Program::from_vec(prog)
    }
  }
  pub fn from_csv(prog_csv: &str) -> Program {
    Self::from_vec(prog_csv.trim().split(',')
                   .map(
                     |s| s.parse::<Value>().unwrap()
                   ).collect())
  }
  pub fn reset(&mut self) {
    self.pc = 0;
    self.rel_base = 0;
    self.cycles = 0;
    self.ints = self.prog.clone();
    self.output = Vec::new();
    self.state = ProgState::STOPPED;
  }
  pub fn run(&mut self) {
    self.reset();
    self.resume();
  }
  pub fn resume(&mut self) -> Option<Value> {
    while {
      self.state = self.step();
      self.state == ProgState::RUNNING} {};

    match self.state {
      ProgState::WRITE => self.get(),
      _ => None
    }
  }
  pub fn dump(&self) {
    let mut it = self.prog.iter();
    loop {
      match self.dump_command(&mut it) {
        Some(cmd) => println!("{}",cmd),
        None => break
      }
    }
  }

  fn dump_command(&self, it: &mut dyn Iterator<Item = &Value>) -> Option<String> {
    let (opcode, modes) = match it.next() {
      Some(p) => Self::parse_opcode(*p),
      None => return None
    };

    let mut params = Vec::new();
    for i in 0..(Self::opsize(opcode)-1) {
      params.push(
        format!("{}{}",
          Self::modename(modes.get(i)), it.next().unwrap()))
    }

    Some(format!("{} {}", Self::opname(opcode), params.join(" ")))
  }

  fn parse_opcode(mut prefix: Value) -> (u8, Vec<u8>) {
    let opcode = (prefix % 100) as u8;
    prefix /= 100;
    let mut modes = Vec::new();
    while prefix > 0 {
      modes.push((prefix % 10) as u8);
      prefix /= 10;
    }
    (opcode, modes)
  }

  fn step(&mut self) -> ProgState {
    self.cycles += 1;
    if self.cycles >= MAX_CYCLES {
      error!("Aborting after {} cycles", self.cycles);
      return ProgState::ABORTED;
    }

    let (opcode, modes) = Self::parse_opcode(self.rr(0));
    self.modes = modes;
    let mut cmd = self.ints[self.pc..].iter();
    let cmdline = format!("{}: {}", self.pc, self.dump_command(&mut cmd).unwrap_or(String::from("DONE")));
    match opcode {
      1 => {
        self.wr(3, self.rrp(1)+self.rrp(2));
        self.adv(opcode);
      },
      2 => {
        self.wr(3, self.rrp(1)*self.rrp(2));
        self.adv(opcode);
      },
      3 => {
        let val = self.get_int();
        match val {
          None => return ProgState::READ,
          Some(val) => {
            self.wr(1, val);
            self.adv(opcode);
          }
        }
      },
      4 => {
        self.put_int(self.rrp(1));
        self.adv(opcode);
      },
      5 => {
        if self.rrp(1) != 0 {
          self.pc = self.rrp(2) as usize;
        } else {
          self.adv(opcode);
        }
      },
      6 => {
        if self.rrp(1) == 0 {
          self.pc = self.rrp(2) as usize;
        } else {
          self.adv(opcode);
        }
      },
      7 => {
        self.wr(3, (self.rrp(1) < self.rrp(2)) as Value);
        self.adv(opcode);
      },
      8 => {
        self.wr(3, (self.rrp(1) == self.rrp(2)) as Value);
        self.adv(opcode);
      },
      9 => {
        self.rel_base += self.rrp(1);
        self.adv(opcode);
      },
      99 => return ProgState::DONE,
      _ => panic!("Unknown opcode: {}", opcode)
    }

    for i in 1..Self::opsize(opcode) {
      match self.modes.get(i-1) {
        Some(0) | Some(2) | None => self.add_recent(self.paddr(i)),
        _ => ()
      }
    }

    let varstr = self.recent_vars.iter().map(
      |(addr, var)| format!("@{}:{}",addr,var)
    ).collect::<Vec<_>>().join(" ");
    trace!("{} ->{}", cmdline, self.ints[self.pc]);
    trace!("R{} {}", self.rel_base, varstr);

    if self.synchronous && opcode == 4 {
      ProgState::WRITE
    } else {
      ProgState::RUNNING
    }
  }

  fn opsize(opcode: u8) -> usize {
    match opcode {
      1|2 => 4,
      3|4 => 2,
      5|6 => 3,
      7|8 => 4,
      9 => 2,
      _ => 1
    }
  }
  fn opname(opcode: u8) -> String {
    match opcode {
      1 => "ADD".to_string(),
      2 => "MUL".to_string(),
      3 => "IN ".to_string(),
      4 => "OUT".to_string(),
      5 => "JNZ".to_string(),
      6 => "JEZ".to_string(),
      7 => "LT ".to_string(),
      8 => "EQ ".to_string(),
      9 => "REL".to_string(),
      99 => "STP".to_string(),
      op => format!("?{:02}", op)
    }
  }
  fn modename(mode: Option<&u8>) -> char {
    match mode {
      Some(0) | None => 'P',
      Some(1) => 'I',
      Some(2) => 'R',
      _ => '?',
    }
  }
  fn adv(&mut self, opcode: u8) {
    self.pc += Self::opsize(opcode);
  }

  pub fn at(&self, pos: usize) -> Value {
    if pos >= self.ints.len() { 0 }
    else { self.ints[pos] }
  }

  fn add_recent(&mut self, addr: usize) {
    self.recent_vars.retain(|(ra, _)| *ra != addr);
    self.recent_vars.push_back((addr, *self.ints.get(addr).unwrap_or(&0)));
    if self.recent_vars.len() > MAX_VARS {
      self.recent_vars.pop_front();
    }
  }

  // Write to an address specified at a given offset
  fn wr(&mut self, dist: usize, val: Value) {
    let addr = self.paddr(dist);
    if addr >= self.ints.len() { self.ints.resize(addr+1, 0); }
    trace!("Writing {} to {}", val, addr);
    self.ints[addr] = val;
  }
  // Read from a value relative to pc
  fn rr(&self, dist: usize) -> Value { self.at(self.pc+dist) }
  // Read from a pointer relative to pc
  fn rrp(&self, dist: usize) -> Value {
    self.at(self.paddr(dist))
  }

  fn paddr(&self, dist: usize) -> usize {
    match self.modes.get(dist-1) {
      Some(0) | None => self.rr(dist) as usize,
      Some(1) => self.pc + dist,
      Some(2) => (self.rel_base + self.rr(dist)) as usize,
      Some(d) => panic!("Unknown mode {}!", d)
    }
  }

  fn get_int(&mut self) -> Option<Value> {
    if self.input.len() == 0 { return None }
    debug!("Got input: {}", self.input[0]);
    Some(self.input.remove(0))
  }
  fn put_int(&mut self, val: Value) {
    debug!("Sent output: {}", val);
    self.output.push(val);
  }

  pub fn get(&mut self) -> Option<Value> {
    match self.output.len() {
      0 => None,
      _ => Some(self.output.remove(0)),
    }
  }
  pub fn put(&mut self, val: Value) { self.input.push(val) }

  pub fn set(&mut self, addr: usize, val: Value) {
    self.ints[addr] = val;
  }
}

pub struct Console {
  prog: Program,
  hooks: HashMap<&'static str, Box<dyn FnMut(String)>>,
}
impl Console {
  pub fn new(mut prog: Program) -> Console {
    Console {
      prog,
      hooks: HashMap::new(),
    }
  }

  pub fn run(&mut self) {
    while self.prog.state != ProgState::DONE {
      self.prog.resume();

      let mut prompt = self.get_response();
      io::stdout().write_all(&prompt.as_bytes());
      self.call_hook("prompt", prompt);

      let mut command = String::new();
      io::stdin().read_line(&mut command);
      self.send_command(&command);
      self.call_hook("command", command);
    }
  }

  pub fn resume(&mut self) { self.prog.resume(); }

  pub fn get_response(&mut self) -> String {
    let mut out = String::new();
    while match self.prog.get() {
      Some(v) => {out.push(v as u8 as char); true}
      None => false
    } {}
    out
  }

  pub fn send_command(&mut self, cmd: &str) {
    for c in cmd.chars() {
      self.prog.put(c as i128);
    }
    if cmd.chars().last() != Some('\n') { self.prog.put(10) }
  }

  pub fn add_hook(&mut self, hook: &'static str, handler: Box<dyn FnMut(String)>) {
    self.hooks.insert(hook, handler);
  }

  fn call_hook(&mut self, name: &str, result: String) {
    match self.hooks.get_mut(name) {
      Some(func) => (*func)(result),
      None => (),
    }
  }
}

#[cfg(test)]
mod intcode_tests {
  use super::*;

  #[test]
  fn basic() {
    let mut prog = Program::from_vec(vec![1002,4,3,4,33]);
    prog.run();
    assert_eq!(prog.at(4), 99);
    debug!("{:?}", prog.output);
  }

  #[test]
  fn negative() {
    let mut prog = Program::from_vec(vec![1101,100,-1,4,0]);
    prog.run();
    assert_eq!(prog.at(4), 99);
  }

  #[test]
  fn zerotest_position() {
    let mut prog = Program::from_vec(
      vec![3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9]);

    prog.input = vec![0];
    prog.run();
    assert_eq!(prog.output, vec![0]);

    prog.input = vec![42];
    prog.run();
    assert_eq!(prog.output, vec![1]);
  }

  #[test]
  fn zerotest_immediate() {
    let mut prog = Program::from_vec(
      vec![3,3,1105,-1,9,1101,0,0,12,4,12,99,1]);

    prog.input = vec![0];
    prog.run();
    assert_eq!(prog.output, vec![0]);

    prog.input = vec![42];
    prog.run();
    assert_eq!(prog.output, vec![1]);
  }

  #[test]
  fn eq8_position() {
    let mut prog = Program::from_vec(
      vec![3,9,8,9,10,9,4,9,99,-1,8]);

    prog.input = vec![8];
    prog.run();
    assert_eq!(prog.output, vec![1]);

    prog.input = vec![42];
    prog.run();
    assert_eq!(prog.output, vec![0]);

    prog.input = vec![-1000];
    prog.run();
    assert_eq!(prog.output, vec![0]);
  }

  #[test]
  fn lt8_position() {
    let mut prog = Program::from_vec(
      vec![3,9,7,9,10,9,4,9,99,-1,8]);

    prog.input = vec![8];
    prog.run();
    assert_eq!(prog.output, vec![0]);

    prog.input = vec![42];
    prog.run();
    assert_eq!(prog.output, vec![0]);

    prog.input = vec![-1000];
    prog.run();
    assert_eq!(prog.output, vec![1]);
  }


  #[test]
  fn eq8_immediate() {
    let mut prog = Program::from_vec(
      vec![3,3,1108,-1,8,3,4,3,99]);

    prog.input = vec![8];
    prog.run();
    assert_eq!(prog.output, vec![1]);

    prog.input = vec![42];
    prog.run();
    assert_eq!(prog.output, vec![0]);

    prog.input = vec![-1000];
    prog.run();
    assert_eq!(prog.output, vec![0]);
  }

  #[test]
  fn lt8_immediate() {
    let mut prog = Program::from_vec(
      vec![3,3,1107,-1,8,3,4,3,99]);

    prog.input = vec![8];
    prog.run();
    assert_eq!(prog.output, vec![0]);

    prog.input = vec![42];
    prog.run();
    assert_eq!(prog.output, vec![0]);

    prog.input = vec![-1000];
    prog.run();
    assert_eq!(prog.output, vec![1]);
  }

  #[test]
  fn cmp8() {
    let mut prog = Program::from_vec(
      vec![
        3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
        1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
        999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99
      ]
    );

    prog.input = vec![8];
    prog.run();
    assert_eq!(prog.output, vec![1000]);

    prog.input = vec![42];
    prog.run();
    assert_eq!(prog.output, vec![1001]);

    prog.input = vec![-1000];
    prog.run();
    assert_eq!(prog.output, vec![999]);
  }

  #[test]
  fn relbase_examples() {
    let ints = vec![109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99];
    let mut prog = Program::from_vec(ints.clone());
    prog.run();
    assert_eq!(prog.output, ints);

    let mut prog = Program::from_vec(
      vec![1102,34915192,34915192,7,4,7,99,0]);
    prog.run();
    assert_eq!(prog.output[0].to_string().len(), 16);

    let ints = vec![104,1125899906842624,99];
    let mut prog = Program::from_vec(ints.clone());
    prog.run();
    assert_eq!(prog.output[0], ints[1]);
  }

  #[test]
  fn failing_opcodes() {
    let mut prog = Program::from_vec(
      vec![109,10,203,10,99]);
    prog.input.push(54321);
    prog.run();
    assert_eq!(prog.at(20), 54321);
  }
}
