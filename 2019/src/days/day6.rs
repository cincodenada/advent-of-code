use std::collections::HashMap;
use crate::util::first_two;

struct System {
  bodies: HashMap<String, OrbitingBody>
}

impl System {
  fn new() -> System {
    let mut s = System {
      bodies: HashMap::new()
    };
    s.bodies.insert("COM".to_string(), OrbitingBody::new());
    s
  }

  fn add_body(&mut self, id: &str, orbits: &str) {
    self.bodies.insert(id.to_string(), OrbitingBody::from_parent(orbits));
  }

  fn checksum(&self) -> u32 {
    let mut sum = 0;
    for (id, _) in &self.bodies {
      sum += self.total_orbits(id);
    }
    sum
  }

  fn total_orbits(&self, id: &str) -> u32 {
    match &self.bodies.get(id).unwrap().orbits {
      Some(parent) => 1 + self.total_orbits(&parent),
      None => 0
    }
  }

  fn orbits<'a>(&'a self, id: &'a str) -> Vec<&'a str> {
    match &self.bodies.get(id).unwrap().orbits {
      Some(parent) => {
        let mut v = self.orbits(&parent);
        v.push(id);
        v
      },
      None => vec![id]
    }
  }

  // Not the most efficient, but it does the job
  fn distance(&self, from: &str, to: &str) -> u32 {
    let from_orbits = self.orbits(from);
    let to_orbits = self.orbits(to);
    let mut from_iter = from_orbits.iter();
    let mut to_iter = to_orbits.iter();

    // Iterate until we find a difference
    while from_iter.next() == to_iter.next() {}
    println!("{:?} vs {:?}", from_iter, to_iter);

    // Distance is number of remaining elements
    // plus the two that we removed with the last iter
    (from_iter.len() + to_iter.len() + 2) as u32
  }
}

struct OrbitingBody {
  orbits: Option<String>, // Only COM is None
}

impl OrbitingBody {
  fn new() -> OrbitingBody {
    OrbitingBody {
      orbits: None,
    }
  }
  fn from_parent(other: &str) -> OrbitingBody {
    let mut body = OrbitingBody::new();
    body.orbits = Some(other.to_string());
    body
  }
}

pub fn run(input: String, part: Option<u8>) -> String {
  let system = build_orbits(
    input.trim()
      .lines().into_iter()
      .map(|s| first_two(s.split(')')) ).collect()
  );

  match part {
    Some(1) => system.checksum(),
    Some(2) | None => system.distance("YOU", "SAN") - 2,
    Some(p) => panic!("Unknown part {}!", p)
  }.to_string()
}

fn build_orbits(orbits: Vec<(&str, &str)>) -> System {
  let mut system = System::new();
  for (center, satellite) in orbits {
    system.add_body(satellite, center);
  }
  
  system
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn example() {
    let sys = build_orbits(vec![
      ("COM","B"),
      ("B","C"),
      ("C","D"),
      ("D","E"),
      ("E","F"),
      ("B","G"),
      ("G","H"),
      ("D","I"),
      ("E","J"),
      ("J","K"),
      ("K","L")
    ]);
    assert_eq!(12, sys.bodies.len());
    assert_eq!(Some(String::from("G")), sys.bodies["H"].orbits);

    assert_eq!(42, sys.checksum())
  }

  #[test]
  fn transfer_example() {
    let sys = build_orbits(vec![
      ("COM","B"),
      ("B","C"),
      ("C","D"),
      ("D","E"),
      ("E","F"),
      ("B","G"),
      ("G","H"),
      ("D","I"),
      ("E","J"),
      ("J","K"),
      ("K","L"),
      ("K","YOU"),
      ("I","SAN")
    ]);

    assert_eq!(6, sys.distance("YOU","SAN"));
  }

  #[test]
  fn transfer_zero() {
    let sys = build_orbits(vec![
      ("COM","YOU"),
      ("COM","SAN")
    ]);

    assert_eq!(2, sys.distance("YOU","SAN"));
  }
}
