use std::collections::HashMap;
use std::collections::hash_set::HashSet;
use std::hash::Hash;
use std::cmp;
use std::fmt;

pub fn minmax<T: Ord + Copy>(a: T, b: T) -> (T, T) { (cmp::min(a, b), cmp::max(a, b)) }

// This turned out...less nice than I wanted
#[derive(Eq, PartialEq, Hash, Copy, Clone, Debug)]
pub struct Point {
  x: i16,
  y: i16
}
impl Point {
  pub fn new(x: i16, y: i16) -> Point { Point{x,y} }
}
impl fmt::Display for Point {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "({}, {})", self.x, self.y)
  }
}

struct Wire {
  // axis, dir (+/-), dist
  segments: Vec<(i16, i16)>
}

impl Wire {
  pub fn new() -> Wire {
    Wire { segments: Vec::new() }
  }

  pub fn from_csv(segs: std::str::Split<'_, char>) -> Wire {
    let mut wire = Wire::new();
    for seg in segs {
      //println!("Processing: {}", mvmt);
      let (dir, num) = seg.split_at(1);
      let dir = dir.chars().next().unwrap();
      let num = num.parse::<i16>().unwrap();

      wire.segments.push(match dir {
        'U' => (0,num),
        'D' => (0,-num),
        'L' => (-num,0),
        'R' => (num,0),
        c => panic!("Unknown direction {}!", c)
      });
    }

    wire
  }

  pub fn dist_to_point(&self, point: &Point) -> Option<u32> {
    let mut pos = Point{ x: 0, y: 0 };
    let mut dist: u32 = 0;

    for (dx, dy) in &self.segments {
      println!("Moving {}, {} from {}", dx, dy, pos);
      let (minx, maxx) = minmax(pos.x, pos.x + dx);
      let (miny, maxy) = minmax(pos.y, pos.y + dy);
      if minx <= point.x && point.x <= maxx && miny <= point.y && point.y <= maxy {
        dist += (point.x - pos.x).abs() as u32;
        dist += (point.y - pos.y).abs() as u32;
        println!("Found, distance: {}", dist);
        return Some(dist);
      } else {
        dist += dx.abs() as u32;
        dist += dy.abs() as u32;
      }
      pos.x += dx;
      pos.y += dy;
    }

    println!("Couldn't find {}!",point); 
    None
  }

  pub fn total_length(&self) -> u32 {
    let mut dist: u32 = 0;

    for (dx, dy) in &self.segments {
      dist += dx.abs() as u32;
      dist += dy.abs() as u32;
    }
    
    dist
  }
}

struct Grid {
  points: HashMap<Point, HashSet<usize>>,
  wires: Vec<Wire>,
}

impl Grid {
  pub fn new() -> Grid {
    Grid {
      points: HashMap::new(),
      wires: Vec::new()
    }
  }

  /*
   * TODO: I gave up on this :(
  pub fn find_intersections(&self)
    -> std::iter::Iterator<Item = (&Point, &HashSet<usize>)> 
    {
    self.points.iter().filter(|(p, wires)| wires.len() >= 2 && !(p.x ==0 && p.y == 0)).collect()
  }
  */

  pub fn draw(&self) -> String {
    let mut max = Point::new(0,0);
    let mut min = Point::new(0,0);
    for (p, _) in &self.points {
      max.x = cmp::max(p.x, max.x);
      max.y = cmp::max(p.y, max.y);
      min.x = cmp::min(p.x, min.x);
      min.y = cmp::min(p.y, min.y);
    }

    min.y -= 1;
    min.x -= 1;
    max.y += 2;
    max.x += 2;

    let mut canvas = String::new();
    for y in min.y..max.y {
      for x in min.x..max.x {
        if x == 0 && y == 0 { canvas.push('o') }
        else {
          canvas.push(match self.getxy(x,y) {
            Some(_) => {
              let nx = self.getxy(x-1, y) != None || self.getxy(x+1, y) != None;
              let ny = self.getxy(x, y-1) != None || self.getxy(x, y-1) != None;
              if nx && ny { '+'  }
              else if nx { '-' }
              else if ny { '|' }
              else { '?' }
            },
            None => '.'
          })
        }
      }
      canvas.push('\n');
    }
    
    canvas
  }

  pub fn add_wire(&mut self, wire: Wire) {
    let mut pos = Point{ x: 0, y: 0 };

    for (xdist, ydist) in &wire.segments {
      if *xdist == 0 {
        for _ in 0..ydist.abs() {
          self.set(self.wires.len(), &pos);
          pos.y += ydist.signum();
        }
      } else if *ydist == 0 {
        for _ in 0..xdist.abs() {
          self.set(self.wires.len(), &pos);
          pos.x += xdist.signum();
        }
      } else {
        panic!("Diagonal wires!!"); 
      }
    }
    // Terminate wire
    self.set(self.wires.len(), &pos);
    self.wires.push(wire);
  }

  pub fn set(&mut self, key: usize, pos: &Point) {
    match self.points.get_mut(&pos) {
      Some(h) => { 
        //println!("Adding at {:?}", pos);
        h.insert(key);
      },
      None => {
        //println!("Inserting at {:?}", pos);
        self.points.insert(*pos, {
          let mut h = HashSet::new();
          h.insert(key);
          h
        });
      }
    };
  }
  pub fn setxy(&mut self, key: usize, x: i16, y: i16) {
    self.set(key, &Point{x, y});
  }

  pub fn get(&self, pos: &Point) -> Option<&HashSet<usize>> { self.points.get(pos) }
  pub fn getxy(&self, x: i16, y: i16) -> Option<&HashSet<usize>> { self.points.get(&Point{x,y}) }
}

pub fn run(input: String, part: Option<u8>) -> String {
  let grid = parse_wirelist(input.trim().lines());

  let (point, dist) = match part {
    Some(1) => get_closest(&grid),
    Some(2) => get_shortest(&grid),
    _ => panic!("Unknown part!")
  };

  println!("Found at point: {:?}", point);
  dist.to_string()
}

fn get_closest(grid: &Grid) -> (Option<&Point>, u32) {
  let mut min_dist = u32::max_value();
  let mut closest = None;

  for (p, wires) in grid.points.iter().filter(
    |(p, wires)| wires.len() >= 2 && !(p.x ==0 && p.y == 0)
  ) {
    println!("Considering {:?} ({:?})", p, wires);
    let dist = (p.x.abs() + p.y.abs()) as u32;
    if dist < min_dist {
      println!("New minimum: {}", dist);
      min_dist = dist;
      closest = Some(p);
    }
  }

  (closest, min_dist)
}

fn get_shortest(grid: &Grid) -> (Option<&Point>, u32) {
  let mut min_dist = u32::max_value();
  let mut shortest = None;

  for (intersection, _) in grid.points.iter().filter(
    |(p, wires)| wires.len() >= 2 && !(p.x ==0 && p.y == 0)
  ) {
    let mut dist = 0;
    let mut found = true;
    println!("Considering {:?}", intersection);
    for wire in &grid.wires {
      match wire.dist_to_point(intersection) {
        Some(d) => { dist += d},
        None => found = false
      }
    }
    if found && (dist as u32) < min_dist {
      min_dist = dist.into();
      shortest = Some(intersection);
    }
  }

  (shortest, min_dist)
}

fn parse_wirelist(wirelist: std::str::Lines) -> Grid {
  let mut grid = Grid::new();
  wirelist.map(|l| {
    println!("{}",l);
    grid.add_wire(Wire::from_csv(l.split(',')));
  }).last();
  grid
}

#[cfg(test)]
mod tests {
  use super::*;

  fn hs(v: Vec<usize>) -> HashSet<usize> { v.iter().cloned().collect() }

  #[test]
  fn populate_grid() {
    let mut grid = Grid::new();

    assert!(grid.getxy(0,0) == None);

    // ........
    // ........
    // ..+---+.
    // ..|...|.
    // ..|...|.
    // ..o...|.
    // ......|.
    // ------+.

    grid.add_wire(Wire::from_csv("U3,R4,D5,L6".split(',')));
    assert_eq!(grid.getxy(0,0), Some(&hs(vec![0])));
    assert_eq!(grid.getxy(0,1), Some(&hs(vec![0])));
    assert_eq!(grid.getxy(0,2), Some(&hs(vec![0])));
    assert_eq!(grid.getxy(0,3), Some(&hs(vec![0])));
    assert_eq!(grid.getxy(1,0), None);
    assert_eq!(grid.getxy(4,3), Some(&hs(vec![0])));
    assert_eq!(grid.getxy(4,2), Some(&hs(vec![0])));
    assert_eq!(grid.getxy(4,1), Some(&hs(vec![0])));
    assert_eq!(grid.getxy(4,0), Some(&hs(vec![0])));
    assert_eq!(grid.getxy(4,-1), Some(&hs(vec![0])));
    assert_eq!(grid.getxy(4,-2), Some(&hs(vec![0])));

    // ........
    // ........
    // ..+---+.
    // ..|.|.|.
    // ..|...|.
    // ..o---+.
    // ......|.
    // ------+.
    grid.add_wire(Wire::from_csv("R4,U3,L2,D1".split(',')));
    assert_eq!(grid.getxy(0,0), Some(&hs(vec![0,1])));
    assert_eq!(grid.getxy(1,0), Some(&hs(vec![1])));
    assert_eq!(grid.getxy(2,0), Some(&hs(vec![1])));
    assert_eq!(grid.getxy(3,0), Some(&hs(vec![1])));
    assert_eq!(grid.getxy(4,0), Some(&hs(vec![0,1])));
    assert_eq!(grid.getxy(5,0), None);
    assert_eq!(grid.getxy(4,3), Some(&hs(vec![0,1])));
    assert_eq!(grid.getxy(4,2), Some(&hs(vec![0,1])));
    assert_eq!(grid.getxy(4,1), Some(&hs(vec![0,1])));
    assert_eq!(grid.getxy(2,2), Some(&hs(vec![1])));
  }

  #[test]
  fn read_lines() {
    parse_wirelist("R8,U5,L5,D3\nU7,R6,D4,L4".trim().lines());
  }

  #[test]
  fn distance_along_wire() {
    let grid = parse_wirelist("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83".trim().lines());

    println!("{}",grid.draw());
    let (_, dist) = get_shortest(&grid);
    assert_eq!(dist, 610);
  }
}
