use crate::util::intcode;
use std::collections::HashMap;

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
struct Coord { x: i32, y: i32 }

struct Robot {
  pos: Coord,
  direction: i8,
}

impl Robot {
  fn new() -> Robot {
    Robot {
      pos: Coord{x: 0, y: 0},
      direction: 0,
    }
  }

  fn rotate(&mut self, dir: u8) {
    self.direction = match dir {
      0 => (self.direction - 1 + 4) % 4,
      1 => (self.direction + 1) % 4,
      _ => panic!("Invalid direction!")
    };
  }
  fn go(&mut self) {
    match self.direction {
      0 => self.pos.y -= 1,
      1 => self.pos.x += 1,
      2 => self.pos.y += 1,
      3 => self.pos.x -= 1,
      _ => panic!("Invalid direction!")
    };
  }
  fn draw(&self) -> char {
    match self.direction {
      0 => '^',
      1 => '>',
      2 => 'v',
      3 => '<',
      _ => '?',
    }
  }
}

struct Hull {
  color: HashMap<Coord, u8>,
  nw: Coord,
  se: Coord,
}
impl Hull {
  fn new() -> Hull {
    Hull {
      color: HashMap::new(),
      nw: Coord{x:0, y:0},
      se: Coord{x:0, y:0},
    }
  }

  fn expand(&mut self, pos: &Coord) {
    if pos.x < self.nw.x { self.nw.x = pos.x }
    if pos.y < self.nw.y { self.nw.y = pos.y }
    if pos.x > self.se.x { self.se.x = pos.x }
    if pos.y > self.se.y { self.se.y = pos.y }
  }

  fn paint(&mut self, pos: &Coord, color: u8) {
    self.expand(pos);
    match self.color.get_mut(pos) {
      Some(v) => *v = color,
      None => {self.color.insert(*pos, color);}
    }
  }
  fn get_color(&self, pos: &Coord) -> u8 { *self.color.get(pos).unwrap_or(&0) }
  fn draw(&self, overlays: Vec<(char, &Coord)>) -> String {
    let overlays = {
      let mut m = HashMap::new();
      for (c, p) in overlays { m.insert(*p, c); }
      m
    };

    let mut out = String::new();
    for y in (self.nw.y-1)..=(self.se.y+1) {
      for x in (self.nw.x-1)..=(self.se.x+1) {
        let pos = Coord{x,y};
        out.push(match overlays.get(&pos) {
          Some(c) => *c,
          _ => match self.color.get(&pos) {
            Some(0) | None => ' ',
            Some(1) => '█',
            _ => panic!("Invalid color!")
          }
        })
      }
      out.push('\n');
    }
    out
  }
}

pub fn run(input: String, part: Option<u8>) -> String {
  let part = part.unwrap_or(2);
  let mut robutt = Robot::new();
  let mut hull = Hull::new();
  let mut prog = intcode::Program::from_csv(&input);
  prog.reset();
  if part == 2 {
    hull.paint(&Coord{x:0,y:0}, 1);
  }

  while prog.state != intcode::ProgState::DONE {
    prog.put(hull.get_color(&robutt.pos) as i128);
    prog.resume();
    process_output(&mut prog.output, &mut robutt, &mut hull);

    debug!("{}[2J{}", 27 as char, hull.draw(vec![(robutt.draw(), &robutt.pos)]));
  }

  match part {
    1 => hull.color.len().to_string(),
    2 => hull.draw(vec![]),
    p => panic!("Unknown part {}!", p)
  }
}

fn process_output(output: &mut Vec<i128>, r: &mut Robot, h: &mut Hull) {
  h.paint(&r.pos, output.remove(0) as u8);
  r.rotate(output.remove(0) as u8);
  r.go();
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn example() {
    let mut robutt = Robot::new();
    let mut hull = Hull::new();

    process_output(&mut vec![1,0], &mut robutt, &mut hull);
    assert_eq!(concat!(
    "   \n",
    "<█ \n",
    "   \n",
    ), hull.draw(vec![(robutt.draw(), &robutt.pos)]));

    process_output(&mut vec![0,0], &mut robutt, &mut hull);
    assert_eq!(concat!(
    "    \n",
    "  █ \n",
    " v  \n",
    ), hull.draw(vec![(robutt.draw(), &robutt.pos)]));

    process_output(&mut vec![1,0], &mut robutt, &mut hull);
    process_output(&mut vec![1,0], &mut robutt, &mut hull);
    assert_eq!(concat!(
    "    \n",
    "  ^ \n",
    " ██ \n",
    "    \n",
    ), hull.draw(vec![(robutt.draw(), &robutt.pos)]));

    process_output(&mut vec![0,1], &mut robutt, &mut hull);
    process_output(&mut vec![1,0], &mut robutt, &mut hull);
    process_output(&mut vec![1,0], &mut robutt, &mut hull);
    assert_eq!(concat!(
    "     \n",
    "  <█ \n",
    "   █ \n",
    " ██  \n",
    "     \n",
    ), hull.draw(vec![(robutt.draw(), &robutt.pos)]));
  }
}
