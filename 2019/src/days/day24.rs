use crate::util::{self, tilemap};
use crate::util::geom::{self, Vec2, Direction};
use std::collections::{HashSet, HashMap};

type BugLevel = tilemap::TileMap<char>;
type BugMap = HashMap<i32, BugLevel>;

pub fn run(input: String, part: Option<u8>) -> String {
  let mut maps = HashMap::new();
  maps.insert(0, tilemap::TileMap::from_str(&input));
  let mut seen = HashSet::new();
  let mut gen = 0;
  let answer = loop {
    if part == Some(1) {
      let checksum = checksum(&maps[&0]);
      if seen.insert(checksum) == false {
        break checksum;
      }
    }
    debug!("=====\nRound {}\n=====\n", gen);
    let mut levels = maps.keys().collect::<Vec<_>>();
    levels.sort();
    let mut lines = Vec::new();
    lines.resize(5, String::new());
    for l in levels {
      let map = maps[&l].draw(|c| *c.unwrap_or(&' '));
      for (idx, l) in map.lines().enumerate() {
        lines[idx].push(' ');
        lines[idx].push_str(l);
      }
    }

    debug!("{}", lines.join("\n"));

    let bugs = age(&mut maps, part == Some(2));
    gen+=1;
    if gen == 200 { break bugs }
  };

  answer.to_string()
}

fn count_edge(map: &BugLevel, dir: &Direction) -> usize {
  let points: Vec<Vec2> = match dir {
    Direction::SOUTH => (0..5).map(|x| Vec2::from_xy(x, 0)).collect(),
    Direction::NORTH => (0..5).map(|x| Vec2::from_xy(x, 4)).collect(),
    Direction::EAST => (0..5).map(|y| Vec2::from_xy(0, y)).collect(),
    Direction::WEST => (0..5).map(|y| Vec2::from_xy(4, y)).collect(),
  };

  let mut count = 0;
  for p in points.iter() {
    match map.get(&p) {
      Some('#') => count+=1,
      _ => (),
    }
  }

  count
}

fn count_center(map: Option<&BugLevel>, dir: Direction) -> usize {
  trace!("Looking {}", dir);
  match map {
    Some(map) => {
      if let Some('#') = match dir {
        Direction::NORTH => map.get(&Vec2::from_xy(2,1)),
        Direction::SOUTH => map.get(&Vec2::from_xy(2,3)),
        Direction::WEST => map.get(&Vec2::from_xy(1,2)),
        Direction::EAST => map.get(&Vec2::from_xy(3,2)),
      } { 1 }
      else { 0 }
    },
    None => 0
  }
}

fn age(maps: &mut BugMap, recursive: bool) -> u32 {
  let center: Vec2 = Vec2::from_xy(2,2);
  let mut updates = Vec::new();
  let mut to_calculate: Vec<i32> = maps.keys().map(|&n| n).collect();
  let mut calculated: HashSet<i32> = HashSet::new();

  let mut total_bugs = 0;


  while to_calculate.len() > 0 {
    let cur_calc = to_calculate.drain(..).collect::<Vec<_>>();
    for l in cur_calc {
      match maps.get(&l) {
        Some(_) => (),
        None => { 
          maps.insert(l, BugLevel::from_str(".....\n.....\n.....\n.....\n.....\n")); 
        }
      }

      let mut outward = false;
      for (&loc, &c) in maps[&l].iter() {
        let mut neighbors = 0;
        for d in geom::DIRECTIONS.iter() {
          let curloc = loc+d.as_vec();
          match maps[&l].get(&curloc) {
            Some(&'#') => neighbors += 1,
            Some(&'.') if curloc == center => {
              match maps.get(&(l+1)) {
                Some(map) => neighbors += count_edge(map, d),
                None => (),
              }
            },
            Some(_) => (),
            None => (),
          }
        }

        let parent = maps.get(&(l-1));
        let mut is_edge = false;
        trace!("Checking outside for {:?}, neighbors before: {}", loc, neighbors);
        if loc.y == 0 {
          is_edge = true;
          neighbors += count_center(parent, Direction::NORTH)
        } else if loc.y == 4 {
          is_edge = true;
          neighbors += count_center(parent, Direction::SOUTH)
        }
        if loc.x == 0 {
          is_edge = true;
          neighbors += count_center(parent, Direction::WEST)
        } else if loc.x == 4 {
          is_edge = true;
          neighbors += count_center(parent, Direction::EAST)
        }
        trace!("Neighbors after: {}", neighbors);

        if is_edge && c == '#' {
          outward = true;
        }

        // Do the actual manipulation and counting
        if loc == center && recursive {
          if neighbors > 0 { to_calculate.push(l+1); }
        } else {
          if c == '#' {
            if neighbors != 1 {
              updates.push((loc, l, '.'));
            } else {
              total_bugs += 1;
            }
          }
          else if c == '.' && (neighbors == 1 || neighbors == 2) {
            updates.push((loc, l, '#'));
            total_bugs += 1;
          }
        }

      }

      if outward { to_calculate.push(l-1) }

      calculated.insert(l);
    }

    if recursive {
      // Remove any maps from the queue that we've already calculated
      // Otherwise we get into an exponentially exploding loop, oops
      to_calculate.retain(|id| calculated.get(id) == None);
    } else {
      to_calculate.clear();
    }
  }

  for (loc, level, c) in updates { maps.get_mut(&level).unwrap().set(loc, c) }
  total_bugs
}

fn checksum(map: &tilemap::TileMap<char>) -> u32 {
  let (min, max) = map.range();
  let mut val = 1;
  let mut total = 0;
  for y in min.y..=max.x {
    for x in min.x..=max.x {
      match map.get_xy(x, y) {
        Some('#') => total += val,
        _ => ()
      }
      val *= 2;
    }
  }
  total
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn example() {
    let mut maps = HashMap::new();
    let mut map = tilemap::TileMap::from_str(&EX1);
    map.axes = false;
    maps.insert(0, map);

    age(&mut maps, false);
    assert_eq!(
"#..#.
####.
###.#
##.##
.##..
", maps[&0].draw(|c| *c.unwrap_or(&' ')));

    age(&mut maps, false);
    assert_eq!(
"#####
....#
....#
...#.
#.###
", maps[&0].draw(|c| *c.unwrap_or(&' ')));

    age(&mut maps, false);
    assert_eq!(
"#....
####.
...##
#.##.
.##.#
", maps[&0].draw(|c| *c.unwrap_or(&' ')));

    age(&mut maps, false);
    assert_eq!(
"####.
....#
##..#
.....
##...
", maps[&0].draw(|c| *c.unwrap_or(&' ')));
  }

  #[test]
  fn example_recursive() {
    let mut maps = HashMap::new();
    maps.insert(0, tilemap::TileMap::from_str(&EX1));

    for i in 0..10 {
      age(&mut maps, true);
    }

    for m in &mut maps.values_mut() {
      m.axes = false;
    }

    assert_eq!(
"..#..
.#.#.
....#
.#.#.
..#..
", maps[&-5].draw(|c| *c.unwrap_or(&' ')));

    assert_eq!(
"...#.
...##
.....
...##
...#.
", maps[&-4].draw(|c| *c.unwrap_or(&' ')));

    assert_eq!(
"#.#..
.#...
.....
.#...
#.#..
", maps[&-3].draw(|c| *c.unwrap_or(&' ')));

    assert_eq!(
".#.##
....#
....#
...##
.###.
", maps[&-2].draw(|c| *c.unwrap_or(&' ')));

    assert_eq!(
"#..##
...##
.....
...#.
.####
", maps[&-1].draw(|c| *c.unwrap_or(&' ')));

    assert_eq!(
".#...
.#.##
.#...
.....
.....
", maps[&0].draw(|c| *c.unwrap_or(&' ')));

    assert_eq!(
".##..
#..##
....#
##.##
#####
", maps[&1].draw(|c| *c.unwrap_or(&' ')));

    assert_eq!(
"###..
##.#.
#....
.#.##
#.#..
", maps[&2].draw(|c| *c.unwrap_or(&' ')));

    assert_eq!(
"..###
.....
#....
#....
#...#
", maps[&3].draw(|c| *c.unwrap_or(&' ')));

    assert_eq!(
".###.
#..#.
#....
##.#.
.....
", maps[&4].draw(|c| *c.unwrap_or(&' ')));

    assert_eq!(
"####.
#..#.
#..#.
####.
.....
", maps[&5].draw(|c| *c.unwrap_or(&' ')));
  }

  const EX1: &str = 
"....#
#..#.
#..##
..#..
#....";

}
