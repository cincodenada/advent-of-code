use crate::util::{intcode, nums};

pub fn run(input: String, part: Option<u8>) -> String {
  let mut progs = Vec::new();
  for _ in 0..5 {
    progs.push(intcode::Program::from_csv(&input));
  }

  let phases = match part {
    Some(1) => (0..=4).collect(),
    Some(2) | None => (5..=9).collect(),
    Some(p) => panic!("Unknown part {}!", p)
  };

  let (_order, output) = find_max(progs, phases);
  output.to_string()
}

fn find_max(mut amps: Vec<intcode::Program>, phases: Vec<u8>) -> (Option<Vec<u8>>, i128) {
  let mut max = 0;
  let mut max_combo = None;
  for phases in nums::permutations(phases) {
    let output = run_amp(&mut amps, &phases);
    if output > max {
      max = output;
      max_combo = Some(phases.clone());
    }
  }
  (max_combo, max)
}

fn every_number(max: u8, length: u8) -> Vec<Vec<u8>> {
  let mut combos: Vec<Vec<u8>> = Vec::new();
  for i in 0..=max {
    if length == 1 {
      combos.push(vec![i]);
    } else {
      for suffix in every_number(max, length-1) {
        combos.push({let mut v = suffix.clone(); v.push(i); v});
      }
    }
  }
  combos
}

fn run_amp(progs: &mut Vec<intcode::Program>, phases: &Vec<u8>) -> i128 {
  let mut result = 0;
  for (idx, p) in progs.iter_mut().enumerate() {
    p.reset();
    p.input.push(phases[idx] as i128);
  }

  while progs.last().unwrap().state != intcode::ProgState::DONE {
    for prog in progs.iter_mut() {
      println!("Input: {}", result);
      prog.input.push(result);
      prog.resume();
      result = prog.output.pop().unwrap();
      println!("Output: {}", result);
    }
  }

  result
}

#[cfg(test)]
mod test {
  use super::*;

  fn make_progs(ints: Vec<i128>) -> Vec<intcode::Program> {
    let mut progs = Vec::new();
    for _ in 0..5 {
      progs.push(intcode::Program::from_vec(ints.clone()));
    }
    progs
  }

  #[test]
  fn examples() {
    let mut progs = make_progs(
      vec![3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0]
    );

    assert_eq!(43210, run_amp(&mut progs, &vec![4,3,2,1,0]));

    let mut progs = make_progs(
      vec![3,23,3,24,1002,24,10,24,1002,23,-1,23,
          101,5,23,23,1,24,23,23,4,23,99,0,0]
    );

    assert_eq!(54321, run_amp(&mut progs, &vec![0,1,2,3,4]));

    let mut progs = make_progs(
      vec![3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
          1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0]
    );

    assert_eq!(65210, run_amp(&mut progs, &vec![1,0,4,3,2]));
  }

  #[test]
  fn everynumber() {
    assert_eq!(vec![
                vec![0,0],vec![1,0],
                vec![0,1],vec![1,1]
              ], every_number(1, 2));

    assert_eq!(vec![
                vec![0,0,0],vec![1,0,0],
                vec![0,1,0],vec![1,1,0],
                vec![0,0,1],vec![1,0,1],
                vec![0,1,1],vec![1,1,1]
              ], every_number(1, 3));

    assert_eq!(vec![
                vec![0,0],vec![1,0],vec![2,0],
                vec![0,1],vec![1,1],vec![2,1],
                vec![0,2],vec![1,2],vec![2,2]
              ], every_number(2, 2));

    assert_eq!(5_usize.pow(5), every_number(4, 5).len());
  }
}
