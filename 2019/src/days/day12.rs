use std::collections::HashMap;
use crate::util;
use crate::util::geom::Vec3;

#[derive(Debug)]
struct Moon {
  pos: Vec3,
  vel: Vec3,
}
impl Moon {
  fn new() -> Moon {
    Moon { pos: Vec3::new(), vel: Vec3::new() }
  }
  fn from_pos(pos: Vec3) -> Moon {
    Moon { pos, ..Moon::new() }
  }

  fn apply_gravity(&mut self, other_pos: &Vec3) {
    self.vel.x += (other_pos.x - self.pos.x).signum();
    self.vel.y += (other_pos.y - self.pos.y).signum();
    self.vel.z += (other_pos.z - self.pos.z).signum();
  }

  fn step(&mut self) {
    self.pos.x += self.vel.x;
    self.pos.y += self.vel.y;
    self.pos.z += self.vel.z;
  }

  fn energy(&self) -> (i32, i32, i32) {
    let pot = self.pos.x.abs() + self.pos.y.abs() + self.pos.z.abs();
    let kin = self.vel.x.abs() + self.vel.y.abs() + self.vel.z.abs();
    (pot, kin, pot * kin)
  }
}

struct System {
  moons: Vec<Moon>
}
impl System {
  fn new(moons: Vec<Moon>) -> System { System { moons } }

  fn step(&mut self) {
    let positions = self.all_pos();
    for m in self.moons.iter_mut() {
      for p in &positions {
        m.apply_gravity(&p);
      }
      m.step();
    }
  }

  fn find_repeat(&mut self) -> u64 {
    let init_x = self.all_x();
    let init_y = self.all_y();
    let init_z = self.all_z();
    let mut repeat_x = 0;
    let mut repeat_y = 0;
    let mut repeat_z = 0;

    let mut steps = 0;
    while repeat_x == 0 || repeat_y == 0 || repeat_z == 0 {
      self.step();
      steps+=1;
      if repeat_x == 0 && init_x == self.all_x() { repeat_x = steps; }
      if repeat_y == 0 && init_y == self.all_y() { repeat_y = steps; }
      if repeat_z == 0 && init_z == self.all_z() { repeat_z = steps; }
    }

    println!("{} {} {}", repeat_x, repeat_y, repeat_z);

    util::nums::lcm(vec![repeat_x, repeat_y, repeat_z])
  }

  fn simulate(&mut self) -> ! {
    let mut steps = 0;
    loop {
      
      println!("{}{}\nSteps: {}", util::cls(), self.draw(), steps);
      self.step();
      steps+=1;
      //util::delay(300);
    }
  }

  fn all_pos(&self) -> Vec<Vec3> {
    self.moons.iter().map(|m| m.pos).collect()
  }
  fn all_vel(&self) -> Vec<Vec3> {
    self.moons.iter().map(|m| m.vel).collect()
  }
  fn all_state(&self) -> Vec<(Vec3, Vec3)> {
    self.moons.iter().map(|m| (m.pos, m.vel)).collect()
  }
  fn all_x(&self) -> Vec<(i32, i32)> { self.moons.iter().map(|m| (m.pos.x, m.vel.x)).collect() }
  fn all_y(&self) -> Vec<(i32, i32)> { self.moons.iter().map(|m| (m.pos.y, m.vel.y)).collect() }
  fn all_z(&self) -> Vec<(i32, i32)> { self.moons.iter().map(|m| (m.pos.z, m.vel.z)).collect() }
  fn all_energy(&self, which: &str) -> Vec<i32> {
    self.moons.iter().map(|m| match which {
      "pot" => m.energy().0,
      "kin" => m.energy().1,
      "total" => m.energy().2,
      _ => panic!("Must be 'pot', 'kin', or 'total'")
    }).collect()
  }

  fn draw(&self) -> String {
    let mut min = Vec3::from_xyz(std::i32::MAX, std::i32::MAX, std::i32::MAX);
    let mut max = Vec3::from_xyz(std::i32::MIN, std::i32::MIN, std::i32::MIN);
    let mut moons: HashMap<(i32, i32), i32> = HashMap::new();
    for p in self.all_pos() {
      if p.x < min.x { min.x = p.x }
      if p.y < min.y { min.y = p.y }
      if p.z < min.z { min.z = p.z }
      if p.x > max.x { max.x = p.x }
      if p.y > max.y { max.y = p.y }
      if p.z > max.z { max.z = p.z }

      match moons.get_mut(&(p.x, p.y)) {
        Some(z) if *z > p.z => *z = p.z,
        None => {moons.insert((p.x, p.y), p.z);},
        _ => ()
      };
    }

    let mut out = String::new();
    for y in min.y..=max.y {
      for x in min.x..=max.x {
        out.push(match moons.get(&(x,y)) {
          Some(z) => {
            match (*z-min.z)*3/(max.z-min.z+1) {
              0 => 'O', 1 => 'o', 2 => '•', _ => '?',
            }
          }
          None => '.',
        });
      }
      out.push('\n');
    }

    out
  }
}

pub fn run(input: String, part: Option<u8>) -> String {
  let mut system = System::new(
    parse_vecs(&input).iter().map(|p| Moon::from_pos(*p)).collect()
  );

  if part == Some(3) { system.simulate() }

  for _ in 0..1000 { system.step(); }

  match part {
    Some(1) => system.all_energy("total").iter().sum::<i32>().to_string(),
    Some(2) | None => system.find_repeat().to_string(),
    Some(p) => panic!("Unknown part {}!", p)
  }
}

fn parse_vecs(input: &str) -> Vec<Vec3> {
  input.trim().split('\n').map(|pos| parse_vec(pos)).collect()
}

fn parse_vec(input: &str) -> Vec3 {
  let mut pos = String::from(input);
  assert!(pos.pop() == Some('>'));
  assert!(pos.remove(0) == '<');
  Vec3::from_iter(&mut pos.split(',').map(
    |coord| coord.split('=').last().unwrap().trim().parse::<i32>().unwrap()
  ))
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn parse() {
    assert_eq!(vec![
      Vec3::from_xyz(-1,0,2),
      Vec3::from_xyz(2,-10,-7),
      Vec3::from_xyz(4,-8,8),
      Vec3::from_xyz(3,5,-1),
    ], parse_vecs(concat![
      "<x=-1, y=0, z=2>\n",
      "<x=2, y=-10, z=-7>\n",
      "<x=4, y=-8, z=8>\n",
      "<x=3, y=5, z=-1>\n",
    ]));
  }

  #[test]
  fn moons() {
    let mut system = System::new(vec![
      Moon::from_pos(Vec3::from_xyz(-10,0,0)),
      Moon::from_pos(Vec3::from_xyz(10,0,0)),
    ]);

    system.step();
    assert_eq!(1, system.moons[0].vel.x);
    assert_eq!(-1, system.moons[1].vel.x);
    assert_eq!(-9, system.moons[0].pos.x);
    assert_eq!(9, system.moons[1].pos.x);
  }

  #[test]
  fn example() {
    let mut system = System::new(parse_vecs(concat![
      "<x=-1, y=0, z=2>\n",
      "<x=2, y=-10, z=-7>\n",
      "<x=4, y=-8, z=8>\n",
      "<x=3, y=5, z=-1>\n",
    ]).iter().map(|p| Moon::from_pos(*p)).collect());

    let steps = parse_example(example1);
    for (i, cur_step) in steps.iter().enumerate() {
      assert_eq!(*cur_step, system.all_state());
      if cur_step != steps.iter().last().unwrap() {
        system.step();
      }
    }
    assert_eq!(179, system.all_energy("total").iter().sum());

    let mut system = System::new(parse_vecs(concat![
      "<x=-8, y=-10, z=0>\n",
      "<x=5, y=5, z=10>\n",
      "<x=2, y=-7, z=3>\n",
      "<x=9, y=-8, z=-3>\n",
    ]).iter().map(|p| Moon::from_pos(*p)).collect());

    let steps = parse_example(example2);
    for (i, cur_step) in steps.iter().enumerate() {
      assert_eq!(*cur_step, system.all_state(),
        "Testing step {}", i*10);
      if cur_step != steps.iter().last().unwrap() {
        for _ in 0..10 { system.step(); }
      }
    }
    assert_eq!(1940, system.all_energy("total").iter().sum());
  }

  #[test]
  fn state_example() {
    let mut system = System::new(parse_vecs(concat![
      "<x=-1, y=0, z=2>\n",
      "<x=2, y=-10, z=-7>\n",
      "<x=4, y=-8, z=8>\n",
      "<x=3, y=5, z=-1>\n",
    ]).iter().map(|p| Moon::from_pos(*p)).collect());

    assert_eq!(2772, system.find_repeat());
  }

  fn parse_example(input: &str) -> Vec<Vec<(Vec3, Vec3)>> {
    let mut steps = Vec::new();
    let mut cur_step = Vec::new();
    for l in input.lines() {
      if l.starts_with("pos") {
        let split = l.find("vel=").expect("Unexpected format");
        cur_step.push((
          parse_vec(&l[4..(split-2)]),
          parse_vec(&l[(split+4)..]),
        ))
      } else if l == "" && cur_step.len() > 0 {
        steps.push(cur_step);
        cur_step = Vec::new();
      }
    }
    if cur_step.len() > 0 { steps.push(cur_step) }
    steps
  }

  const example1: &str = "
After 0 steps:
pos=<x=-1, y=  0, z= 2>, vel=<x= 0, y= 0, z= 0>
pos=<x= 2, y=-10, z=-7>, vel=<x= 0, y= 0, z= 0>
pos=<x= 4, y= -8, z= 8>, vel=<x= 0, y= 0, z= 0>
pos=<x= 3, y=  5, z=-1>, vel=<x= 0, y= 0, z= 0>

After 1 step:
pos=<x= 2, y=-1, z= 1>, vel=<x= 3, y=-1, z=-1>
pos=<x= 3, y=-7, z=-4>, vel=<x= 1, y= 3, z= 3>
pos=<x= 1, y=-7, z= 5>, vel=<x=-3, y= 1, z=-3>
pos=<x= 2, y= 2, z= 0>, vel=<x=-1, y=-3, z= 1>

After 2 steps:
pos=<x= 5, y=-3, z=-1>, vel=<x= 3, y=-2, z=-2>
pos=<x= 1, y=-2, z= 2>, vel=<x=-2, y= 5, z= 6>
pos=<x= 1, y=-4, z=-1>, vel=<x= 0, y= 3, z=-6>
pos=<x= 1, y=-4, z= 2>, vel=<x=-1, y=-6, z= 2>

After 3 steps:
pos=<x= 5, y=-6, z=-1>, vel=<x= 0, y=-3, z= 0>
pos=<x= 0, y= 0, z= 6>, vel=<x=-1, y= 2, z= 4>
pos=<x= 2, y= 1, z=-5>, vel=<x= 1, y= 5, z=-4>
pos=<x= 1, y=-8, z= 2>, vel=<x= 0, y=-4, z= 0>

After 4 steps:
pos=<x= 2, y=-8, z= 0>, vel=<x=-3, y=-2, z= 1>
pos=<x= 2, y= 1, z= 7>, vel=<x= 2, y= 1, z= 1>
pos=<x= 2, y= 3, z=-6>, vel=<x= 0, y= 2, z=-1>
pos=<x= 2, y=-9, z= 1>, vel=<x= 1, y=-1, z=-1>

After 5 steps:
pos=<x=-1, y=-9, z= 2>, vel=<x=-3, y=-1, z= 2>
pos=<x= 4, y= 1, z= 5>, vel=<x= 2, y= 0, z=-2>
pos=<x= 2, y= 2, z=-4>, vel=<x= 0, y=-1, z= 2>
pos=<x= 3, y=-7, z=-1>, vel=<x= 1, y= 2, z=-2>

After 6 steps:
pos=<x=-1, y=-7, z= 3>, vel=<x= 0, y= 2, z= 1>
pos=<x= 3, y= 0, z= 0>, vel=<x=-1, y=-1, z=-5>
pos=<x= 3, y=-2, z= 1>, vel=<x= 1, y=-4, z= 5>
pos=<x= 3, y=-4, z=-2>, vel=<x= 0, y= 3, z=-1>

After 7 steps:
pos=<x= 2, y=-2, z= 1>, vel=<x= 3, y= 5, z=-2>
pos=<x= 1, y=-4, z=-4>, vel=<x=-2, y=-4, z=-4>
pos=<x= 3, y=-7, z= 5>, vel=<x= 0, y=-5, z= 4>
pos=<x= 2, y= 0, z= 0>, vel=<x=-1, y= 4, z= 2>

After 8 steps:
pos=<x= 5, y= 2, z=-2>, vel=<x= 3, y= 4, z=-3>
pos=<x= 2, y=-7, z=-5>, vel=<x= 1, y=-3, z=-1>
pos=<x= 0, y=-9, z= 6>, vel=<x=-3, y=-2, z= 1>
pos=<x= 1, y= 1, z= 3>, vel=<x=-1, y= 1, z= 3>

After 9 steps:
pos=<x= 5, y= 3, z=-4>, vel=<x= 0, y= 1, z=-2>
pos=<x= 2, y=-9, z=-3>, vel=<x= 0, y=-2, z= 2>
pos=<x= 0, y=-8, z= 4>, vel=<x= 0, y= 1, z=-2>
pos=<x= 1, y= 1, z= 5>, vel=<x= 0, y= 0, z= 2>

After 10 steps:
pos=<x= 2, y= 1, z=-3>, vel=<x=-3, y=-2, z= 1>
pos=<x= 1, y=-8, z= 0>, vel=<x=-1, y= 1, z= 3>
pos=<x= 3, y=-6, z= 1>, vel=<x= 3, y= 2, z=-3>
pos=<x= 2, y= 0, z= 4>, vel=<x= 1, y=-1, z=-1>
";

  const example2: &str = "
After 0 steps:
pos=<x= -8, y=-10, z=  0>, vel=<x=  0, y=  0, z=  0>
pos=<x=  5, y=  5, z= 10>, vel=<x=  0, y=  0, z=  0>
pos=<x=  2, y= -7, z=  3>, vel=<x=  0, y=  0, z=  0>
pos=<x=  9, y= -8, z= -3>, vel=<x=  0, y=  0, z=  0>

After 10 steps:
pos=<x= -9, y=-10, z=  1>, vel=<x= -2, y= -2, z= -1>
pos=<x=  4, y= 10, z=  9>, vel=<x= -3, y=  7, z= -2>
pos=<x=  8, y=-10, z= -3>, vel=<x=  5, y= -1, z= -2>
pos=<x=  5, y=-10, z=  3>, vel=<x=  0, y= -4, z=  5>

After 20 steps:
pos=<x=-10, y=  3, z= -4>, vel=<x= -5, y=  2, z=  0>
pos=<x=  5, y=-25, z=  6>, vel=<x=  1, y=  1, z= -4>
pos=<x= 13, y=  1, z=  1>, vel=<x=  5, y= -2, z=  2>
pos=<x=  0, y=  1, z=  7>, vel=<x= -1, y= -1, z=  2>

After 30 steps:
pos=<x= 15, y= -6, z= -9>, vel=<x= -5, y=  4, z=  0>
pos=<x= -4, y=-11, z=  3>, vel=<x= -3, y=-10, z=  0>
pos=<x=  0, y= -1, z= 11>, vel=<x=  7, y=  4, z=  3>
pos=<x= -3, y= -2, z=  5>, vel=<x=  1, y=  2, z= -3>

After 40 steps:
pos=<x= 14, y=-12, z= -4>, vel=<x= 11, y=  3, z=  0>
pos=<x= -1, y= 18, z=  8>, vel=<x= -5, y=  2, z=  3>
pos=<x= -5, y=-14, z=  8>, vel=<x=  1, y= -2, z=  0>
pos=<x=  0, y=-12, z= -2>, vel=<x= -7, y= -3, z= -3>

After 50 steps:
pos=<x=-23, y=  4, z=  1>, vel=<x= -7, y= -1, z=  2>
pos=<x= 20, y=-31, z= 13>, vel=<x=  5, y=  3, z=  4>
pos=<x= -4, y=  6, z=  1>, vel=<x= -1, y=  1, z= -3>
pos=<x= 15, y=  1, z= -5>, vel=<x=  3, y= -3, z= -3>

After 60 steps:
pos=<x= 36, y=-10, z=  6>, vel=<x=  5, y=  0, z=  3>
pos=<x=-18, y= 10, z=  9>, vel=<x= -3, y= -7, z=  5>
pos=<x=  8, y=-12, z= -3>, vel=<x= -2, y=  1, z= -7>
pos=<x=-18, y= -8, z= -2>, vel=<x=  0, y=  6, z= -1>

After 70 steps:
pos=<x=-33, y= -6, z=  5>, vel=<x= -5, y= -4, z=  7>
pos=<x= 13, y= -9, z=  2>, vel=<x= -2, y= 11, z=  3>
pos=<x= 11, y= -8, z=  2>, vel=<x=  8, y= -6, z= -7>
pos=<x= 17, y=  3, z=  1>, vel=<x= -1, y= -1, z= -3>

After 80 steps:
pos=<x= 30, y= -8, z=  3>, vel=<x=  3, y=  3, z=  0>
pos=<x= -2, y= -4, z=  0>, vel=<x=  4, y=-13, z=  2>
pos=<x=-18, y= -7, z= 15>, vel=<x= -8, y=  2, z= -2>
pos=<x= -2, y= -1, z= -8>, vel=<x=  1, y=  8, z=  0>

After 90 steps:
pos=<x=-25, y= -1, z=  4>, vel=<x=  1, y= -3, z=  4>
pos=<x=  2, y= -9, z=  0>, vel=<x= -3, y= 13, z= -1>
pos=<x= 32, y= -8, z= 14>, vel=<x=  5, y= -4, z=  6>
pos=<x= -1, y= -2, z= -8>, vel=<x= -3, y= -6, z= -9>

After 100 steps:
pos=<x=  8, y=-12, z= -9>, vel=<x= -7, y=  3, z=  0>
pos=<x= 13, y= 16, z= -3>, vel=<x=  3, y=-11, z= -5>
pos=<x=-29, y=-11, z= -1>, vel=<x= -3, y=  7, z=  4>
pos=<x= 16, y=-13, z= 23>, vel=<x=  7, y=  1, z=  1>
";

}
