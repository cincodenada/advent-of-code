use primes::PrimeSet;
use std::cmp;
use num_rational::Ratio;
use std::collections::HashMap;

type Coord = (i32, i32);
type Field<'a> = Vec<&'a str>;

type LocKey = (i8, Ratio<i32>);
type LocMap = HashMap<LocKey, Vec<LocationInfo>>;

struct LocationInfo {
  to: Coord,
  angle: LocKey,
  distance: u32, // Manhattan
}

impl LocationInfo {
  fn new(from: &Coord, to: &Coord) -> LocationInfo {
    let vector = (to.0 - from.0, to.1 - from.1);
    LocationInfo {
      to: to.clone(),
      distance: (vector.0.abs() + vector.1.abs()) as u32,
      /*
       * So, because the y axis goes to infinity, we do some tricksy shit here
       * The first element is a "quadrant": negative y axis (up) is 0,
       * 0 - negative y axis (up)
       * 1 - right side of the field
       * 2 - positive y axis (down)
       * 3 - left side of the field
       *
       * So when we sort by quadrant and then angle, everything shakes out
       *
       * Also, we replace 0 with 1 in the denominator because if dx is 0
       * then the angle doesn't matter because it's all the same
       */
      angle: (
        match vector.0 {
          0 => match vector.1 { y if y < 0 => 0, _ => 2 },
          x if x > 0 => 1,
          x if x < 0 => 3,
          // I don't know why this is necessary 🤔
          _ => panic!("That should be everything")
        },
        Ratio::new(vector.1, match vector.0 { 0 => vector.1, x => x }),
      )
    }
  }
}

pub fn run(input: String, part: Option<u8>) -> String {
  let field = input.trim().split("\n").collect();

  let (best, count) = best_asteroid(&field);
  chart_visible(&field, &best);

  match part {
    Some(1) => count,
    Some(2) | None => {
      let order = vaporize(angle_map(&field, &best));
      (order[199].0*100 + order[199].1) as u32
    },
    Some(p) => panic!("Unknown part {}!", p)
  }.to_string()
}

fn vaporize(mut map: LocMap) -> Vec<Coord> {
  let mut order = Vec::new();
  let mut last_total = 99;
  let mut keys = map.keys().map(|k| k.clone()).collect::<Vec<LocKey>>();
  keys.sort();
  while order.len() != last_total {
    last_total = order.len();
    for k in keys.iter_mut() {
      let remaining = map.get_mut(k).unwrap();
      if remaining.len() > 0 {
        let victim = remaining.remove(0);
        order.push(victim.to);
      }
    }
  }
  order
}

fn angle_map(field: &Field, from: &Coord) -> LocMap {
  let asteroids = find_char('#', field);
  let mut map = LocMap::new();

  for a in asteroids.iter().filter(|a| *a != from) {
    let loc = LocationInfo::new(from, a);
    match map.get_mut(&loc.angle) {
      Some(v) => v.push(loc),
      None => {map.insert(loc.angle, vec![loc]);}
    }
  }

  for locs in map.values_mut() {
    locs.sort_by_key(|loc| loc.distance);
  }

  map
}

fn size(field: &Field) -> (usize, usize) {
  (field[0].len(), field.len())
}

fn best_asteroid(field: &Field) -> (Coord, u32) {
  let asteroids = find_char('#', field);

  let mut max = 0;
  let mut best = None;
  for a in asteroids.iter() {
    let visible = visible(size(field), &asteroids, a);
    let count = (visible.len() - 1) as u32;
    if count > max {
      max = count;
      best = Some(a.clone());
    }
  }

  (best.unwrap(), max)
}

// Debug
fn chart_visible(field: &Field, from: &Coord) {
  let asteroids = find_char('#', field);
  let visible = visible(size(field), &asteroids, from);
  println!("Looking from {:?}:", from);
  for (y, row) in field.iter().enumerate() {
    let mut out = String::new();
    for (x, c) in row.chars().enumerate() {
      let pos = (x as i32, y as i32);
      out.push(match c {
        '.' => '.',
        _ if pos == *from => 'O',
        _ if visible.iter().find(|c| **c == pos) == None => 'X',
        _ => '#'
      });
    }
    println!("{}", out);
  }
}

fn visible(field_size: (usize, usize), asteroids: &Vec<Coord>, from: &Coord) -> Vec<Coord> {
  let mut remaining = asteroids.clone();
  // Run through other asteroids and remove those occluded by them
  for other in asteroids.iter().filter(|a| *a != from) {
    let occluded = blind_spots(field_size, from, other);
    trace!("Occluded spots for {:?}: {:?}", other, occluded);
    remaining.retain(|a| occluded.iter().find(|occ| *occ == a) == None);
  }

  remaining
}

fn find_char(l: char, field: &Field) -> Vec<Coord> {
  let mut locs = Vec::new();
  for (y, row) in field.iter().enumerate() {
    for (x, c) in row.chars().enumerate() {
      if c == l { locs.push((x as i32, y as i32)) }
    }
  }
  locs
}

fn reduced_vector(from: &Coord, to: &Coord) -> Coord {
  let mut primes = PrimeSet::new();

  let mut dist = (to.0 - from.0, to.1 - from.1);

  for p in primes.iter() {
    trace!("Checking {} into {:?}...", p, dist);
    let p = p as i32;
    // This could be min except for 0's :(
    if p > cmp::max(dist.0.abs(),dist.1.abs()) { break }
    while dist.0 % p == 0 && dist.1 % p == 0 {
      trace!("Dividing {:?} by {}...", dist, p);
      dist = (dist.0/p, dist.1/p);
    }
  }

  dist
}

fn blind_spots(field_size: (usize, usize), from: &Coord, to: &Coord) -> Vec<Coord> {
  let minvec = reduced_vector(from, to);
  debug!("Going {:?} from {:?}", minvec, to);

  let mut spots = Vec::new();
  let mut cur_spot = to.clone();
  loop {
    cur_spot.0 += minvec.0;
    cur_spot.1 += minvec.1;
    if cur_spot.0 >= field_size.0 as i32
      || cur_spot.1 >= field_size.1 as i32
      || cur_spot.0 < 0
      || cur_spot.1 < 0
      { break }
    spots.push(cur_spot.clone());
  }

  spots
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn reduced() {
    assert_eq!((1,1), reduced_vector(&(0,0),&(50,50)));
    assert_eq!((-2,1), reduced_vector(&(0,0),&(-50,25)));
    assert_eq!((-7,-2), reduced_vector(&(0,0),&(-5670,-1620)));
    assert_eq!((0,1), reduced_vector(&(0,0),&(0,100)));
    assert_eq!((-1,0), reduced_vector(&(2,0),&(0,0)));
  }

  #[test]
  fn occlusion() {
    let examples = vec!(
      "#....Iiiii",
      "H..A......",
      "h..B..a...",
      "hEDCG....a",
      "h.F.c.b...",
      "h....c....",
      "h.efd.c.gb",
      "h......c..",
      "h...f...c.",
      "h..e..d..c",
    );

    assert_eq!(vec![(3,1)], find_char('A', &examples));
    assert_eq!(vec![(6,2),(9,3)], find_char('a', &examples));

    for (u, l) in (0..8).map(|n| (('A' as u8 + n) as char, (('a' as u8) + n) as char)) {
      let occluded = blind_spots(size(&examples), &(0,0), &find_char(u, &examples)[0]);
      assert_eq!(occluded, find_char(l, &examples));
    }
  }

  #[test]
  fn examples() {
    let field = vec!(
      "......#.#.",
      "#..#.#....",
      "..#######.",
      ".#.#.###..",
      ".#..#.....",
      "..#....#.#",
      "#..#....#.",
      ".##.#..###",
      "##...#..#.",
      ".#....####",
    );
    chart_visible(&field, &(5,8));
    chart_visible(&field, &(4,7));
    assert_eq!(((5,8), 33), best_asteroid(&field));
  }

  #[test]
  fn angle_test() {
    let field = vec!(
      "###",
      "###",
      "###",
      "###",
    );

    let map = angle_map(&field, &(1,1));

    let key = |q, x, y| {
      (q, Ratio::new(y, match x { 0 => y, _ => x }))
    };

    assert_eq!(
      vec![(1,0)],
      map[&key(0, 0, -1)].iter().map(|l| l.to).collect::<Vec<Coord>>()
    );
    assert_eq!(
      vec![(1,2),(1,3)],
      map[&key(2, 0, 1)].iter().map(|l| l.to).collect::<Vec<Coord>>()
    );

    let mut keys = map.keys().collect::<Vec<&LocKey>>();
    keys.sort();
    assert_eq!(vec![
      &key(0, 0, -1),
      &key(1, 1, -1),
      &key(1, 1, 0),
      &key(1, 1, 1),
      &key(1, 1, 2),
      &key(2, 0, 1),
      &key(3, -1, 2),
      &key(3, -1, 1),
      &key(3, -1, 0),
      &key(3, -1, -1),
    ], keys);

    assert_eq!(vec![
      (1,0),
      (2,0),
      (2,1),
      (2,2),
      (2,3),
      (1,2),
      (0,3),
      (0,2),
      (0,1),
      (0,0),
      (1,3),
    ], vaporize(map))
  }
}
