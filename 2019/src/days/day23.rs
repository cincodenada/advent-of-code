use crate::util::{self, intcode};
use std::sync::{Mutex, Arc};
use std::thread;
use std::sync::mpsc;
use std::collections::HashMap;
use std::cmp;

type Packet = (intcode::Value, intcode::Value);

const NAT_ID: usize = 255;
const IDLE_ID: usize = 1024;

struct Computer {
  prog: intcode::Program,
  id: usize,
  tx: mpsc::Sender<Packet>,
  rx: mpsc::Receiver<Packet>,
  channels: HashMap<usize, mpsc::Sender<Packet>>,
}
impl Computer {
  fn new(prog: &str, id: usize) -> Computer {
    let (tx, rx) = mpsc::channel();
    Computer {
      id, tx, rx,
      channels: HashMap::new(),
      prog: intcode::Program::from_csv(prog),
    }
  }

  fn run(&mut self) {
    self.boot();
    loop {
      self.step();
    }
  }

  fn boot(&mut self) {
    println!("Booting up computer {}", self.id);
    self.prog.synchronous = true;
    self.prog.put(self.id as intcode::Value);
  }

  fn step(&mut self) -> bool {
    let result = self.prog.resume();
    match &self.prog.state {
      intcode::ProgState::READ => {
        match self.rx.try_recv() {
          Ok((x, y)) => {
            trace!("{} recieved packet", self.id);
            self.prog.put(x);
            self.prog.put(y);
            self.channels[&IDLE_ID].send((self.id as i128, 2));
            true
          },
          Err(err) => {
            self.channels[&IDLE_ID].send((self.id as i128, 0));
            self.prog.put(-1);
            false
          }
        }
      },
      intcode::ProgState::WRITE => {
        let addr = result.expect("Output not found!");
        let x = self.prog.resume().expect("X not provided!");
        let y = self.prog.resume().expect("Y not provided!");
        self.channels[&(addr as usize)].send((x, y));
        self.channels[&IDLE_ID].send((self.id as i128, 1));
        trace!("{} sent packet to {}", self.id, addr);
        true
      },
      other => panic!("Unexpected state {:?}", other),
    }
  }

  fn add_peer(&mut self, id: usize, channel: mpsc::Sender<Packet>) {
    self.channels.insert(id, channel);
  }

  fn get_tx(&self) -> mpsc::Sender<Packet> { self.tx.clone() }
}

struct Network {
  members: Vec<mpsc::Sender<Packet>>,
}
impl Network {
  fn new() -> Network { Network { members: Vec::new() } }

  fn add_member(&mut self, tx: mpsc::Sender<Packet>) {
    self.members.push(tx);
  }
}

pub fn run(input: String, part: Option<u8>) -> String {
  let num_comps = 50;
  let mut computers = Vec::with_capacity(num_comps);
  let mut threads = Vec::with_capacity(num_comps);

  for id in 0..num_comps {
    let compy = Computer::new(&input, id);
    computers.push(compy);
  }

  let handles = computers.iter().map(|c| c.get_tx()).collect::<Vec<_>>();
  let (nat_tx, nat_rx) = mpsc::channel();
  let (idle_tx, idle_rx) = mpsc::channel();

  for c in &mut computers {
    for (id, h) in handles.iter().enumerate() {
      c.add_peer(id, h.clone()); 
    }
    c.add_peer(NAT_ID, nat_tx.clone());
    c.add_peer(IDLE_ID, idle_tx.clone());
  }

  let threaded = false;

  let answer = if threaded {
    computers.drain(..).enumerate().map(|(idx, mut c)|
      threads.push(
        thread::Builder::new()
          .name(format!("Computer {}", idx))
          .spawn(move || {
            c.run();
          }).unwrap()
      )
    ).count();

    match part {
      Some(1) => {
        nat_rx.recv().unwrap().1
      },
      Some(2) => {
        let monitor_handle = thread::Builder::new().name(String::from("Monitor"))
          .spawn(build_monitor(num_comps, idle_rx, nat_rx, handles[0].clone()))
          .unwrap();
        monitor_handle.join().unwrap()
      },
      _ => panic!("Wrong part!!")
    }
  } else {
    let mut last_y = None;
    let mut last_active = 0;

    for c in &mut computers { c.boot() }

    loop {
      let mut active = 0;
      for c in &mut computers {
        if c.step() { active += 1 };
      }

      match part {
        Some(1) => {
          match nat_rx.try_recv() {
            Ok(val) => break val.1,
            Err(_) => ()
          }
        },
        Some(2) => {
          if active == 0 && last_active != 0 {
            let mut nat_value = None;
            while let Ok(val) = nat_rx.try_recv() {
              nat_value = Some(val);
            }

            match nat_value {
              Some(val) => {
                handles[0].send(val);
                if Some(val.1) == last_y {
                  //println!("Found duplicate y: {}", val.1);
                  break val.1;
                } else {
                  last_y = Some(val.1);
                }
              },
              None => panic!("No NAT value to send!"),
            };
          }

          last_active = active;
        },
        _ => panic!("Wrong part!!")
      }
    }
  };

  answer.to_string()
}

fn build_monitor(
  num_comps: usize,
  monitor_rx: mpsc::Receiver<Packet>,
  nat_rx: mpsc::Receiver<Packet>,
  kickstart_tx: mpsc::Sender<Packet>) -> impl Fn() -> i128 {
  move || {
    let mut has_started = false;
    let mut nat_value = None;

    let mut send = Vec::with_capacity(num_comps);
    let mut recv = Vec::with_capacity(num_comps);
    let mut idle = Vec::with_capacity(num_comps);
    send.resize(num_comps, 0);
    recv.resize(num_comps, 0);
    idle.resize(num_comps, 0);

    const BLIP_LEN: usize = 100;
    const DISPLAY_EVERY: usize = 50;

    let mut last_y = None;
    let mut kickstart_blip = 0;
    let mut kickstart_msg = String::new();
    let mut idle_len = 0;
    let mut display_countdown = DISPLAY_EVERY;
    loop {
      while let Ok(val) = monitor_rx.try_recv() {
        match val {
          (id, 0) => idle[id as usize]=BLIP_LEN,
          (id, 1) => {
            send[id as usize]=BLIP_LEN;
            idle[id as usize]=0;
          },
          (id, 2) => {
            recv[id as usize]=BLIP_LEN;
            idle[id as usize]=0;
          },
          (id, n) => panic!("Unexpected monitor value {}"),
        }
      }
      if display_countdown == 0 {
        let total_send: usize = send.iter().sum();
        let total_recv: usize = recv.iter().sum();
        let total_idle: usize = idle.iter().sum();
        println!("{}Send: {} | {}\nRecv: {} | {}\nIdle: {} | {}", util::cls(),
          send.iter().map(|&v| if v > 0 { 'x' } else { '.' }).collect::<String>(),
          total_send,
          recv.iter().map(|&v| if v > 0 { 'x' } else { '.' }).collect::<String>(),
          total_recv,
          idle.iter().map(|&v| if v > 0 { 'x' } else { '.' }).collect::<String>(),
          total_idle,
        );
        if kickstart_blip > 0 {
          println!("{}", kickstart_msg);
        } else {
          println!("");
        }
        display_countdown = DISPLAY_EVERY;
      } else {
        display_countdown -= 1
      }

      for v in send.iter_mut() { if *v > 0 { *v-=1; } }
      for v in recv.iter_mut() { if *v > 0 { *v-=1; } }
      for v in idle.iter_mut() { if *v > 0 { *v-=1; } }
      if kickstart_blip > 0 { kickstart_blip -= 1; }

      let idle_count = idle.iter().filter(|&x| *x > 0).count();
      if idle_count == idle.len() {
        if has_started { idle_len += 1 }
      } else {
        idle_len = 0;
        has_started = true;
      }

      if idle_len > 5 {
        // Drain the NAT queue
        while let Ok(val) = nat_rx.try_recv() {
          nat_value = Some(val);
        }

        match nat_value {
          Some(val) => {
            kickstart_msg = format!("Sent kickstart: {:?}", val);
            kickstart_blip = BLIP_LEN*10;
            kickstart_tx.send(val);
            if Some(val.1) == last_y {
              println!("Found duplicate y: {}", val.1);
              break val.1;
            } else {
              last_y = Some(val.1);
            }
          },
          None => panic!("No NAT value to send!"),
        };

        has_started = false;
        idle_len = 0;
      }

      util::delay(1);
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;
}
