pub fn run(input: String, part: Option<u8>) -> String {
  let range: Vec<u32> = input.trim().split('-').map(|s| s.parse::<u32>().unwrap()).collect();

  let max_rep = if part == Some(1) { 6 } else { 2 };
  let mut valid_count = 0;
  for n in range[0]..(range[1]+1) {
    // Assume it's six digits
    let text = n.to_string();
    let chars = text.chars();
    if validate(chars, max_rep) { valid_count += 1; }
  }

  valid_count.to_string()
}

pub fn validate(mut chars: std::str::Chars, max_rep: u8) -> bool {
  let mut last_char = chars.next().unwrap();
  let mut rep: bool = false;
  let mut cur_rep = 1;
  for c in chars {
    if c < last_char { return false; }
    if c == last_char {
      cur_rep += 1;
    } else if cur_rep > 1 {
      rep = rep || (cur_rep <= max_rep);
      cur_rep = 1;
    }
    last_char = c;
  }
  rep = rep || (cur_rep > 1 && cur_rep <= max_rep);

  rep
}
