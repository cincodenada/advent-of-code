use std::char;
use std::iter;
use std::collections::HashMap;
const BASE: [i32; 4] = [0, 1, 0, -1];

pub fn run(input: String, part: Option<u8>) -> String {
  let input_list = arr_from_str(&input.trim());
  let len = input_list.len();
  let output = match part {
    Some(1) => fft_loop(input_list, &build_factors(len, 1), 100),
    Some(2) | None => fft_loop(input_list, &build_factors(len, 10_000), 100),
    Some(p) => panic!("Unknown part {}!", p)
  };

  str_from_arr(&output)[0..8].to_string()
}

pub fn arr_from_str(list: &str) -> Vec<u32>{
  list.chars().map(|c| c.to_digit(10).unwrap() as u32).collect()
}
fn str_from_arr(list: &Vec<u32>) -> String {
  list.iter().map(|d| char::from_digit(*d, 10).unwrap())
  .collect::<String>()
}


struct BasePattern {
  rep_count: usize,
  cur_rep: usize,
  cur_idx: usize,
}
impl BasePattern {
  fn new(rep_count: usize) -> BasePattern {
    BasePattern {
      rep_count,
      cur_rep: 1,
      cur_idx: 0,
    }
  }
}
impl Iterator for BasePattern {
  type Item = i32;
  fn next(&mut self) -> Option<Self::Item> {
    if self.cur_rep == self.rep_count {
      self.cur_rep = 0;
      self.cur_idx = (self.cur_idx + 1) % 4
    }
    self.cur_rep+=1;
    Some(BASE[self.cur_idx])
  }
}

#[derive(Clone)]
struct RepeatedList<'a> {
  list: &'a Vec<u32>,
  repeats: usize,
  cur_rep: usize,
  cur_iter: std::slice::Iter<'a, u32>,
}
impl<'a> RepeatedList<'a> {
  fn from_vec(list: &'a Vec<u32>, repeats: usize) -> RepeatedList<'a> {
    RepeatedList {
      repeats,
      list,
      cur_rep: 0,
      cur_iter: list.iter(),
    }
  }
}
impl<'a> Iterator for RepeatedList<'a> {
  type Item = u32;

  fn next(&mut self) -> Option<Self::Item> {
    match self.cur_iter.next() {
      Some(&val) => Some(val),
      None => {
        self.cur_iter = self.list.iter();
        self.cur_rep += 1;
        if self.cur_rep == self.repeats {
          None
        } else {
          Some(*self.cur_iter.next().unwrap())
        }
      }
    }
  }
}
impl<'a> ExactSizeIterator for RepeatedList<'a> {
  fn len(&self) -> usize { self.list.len()*self.repeats }
}

fn last_digit(num: i32) -> u32 { (num % 10).abs() as u32 }

fn build_factors(len: usize, repeats: u32) -> Vec<Vec<i32>> {
  let mut rows = Vec::new();
  let total_len = len*repeats as usize;
  rows.reserve(total_len);
  for row in 0..total_len {
    let mut mult = BasePattern::new(row+1);
    let mut cols: Vec<i32> = iter::repeat(0).take(len).collect();
    for rep in 0..repeats {
      for col in 0..len {
        cols[col] += mult.next().unwrap();
      }
    }
    rows.push(cols);
  }
  rows
}

fn fft<'a, I, J>(list: &I, factors: &J) -> Vec<u32>
where I: ExactSizeIterator<Item = &'a u32> + Clone,
      J: ExactSizeIterator<Item = &'a Vec<i32>> + Clone {
  let mut output = Vec::new();
  for cur_factors in factors.clone() {
    let mut partial_sums: HashMap<i32, u32> = HashMap::new();
    for (&factor, &val) in cur_factors.iter().zip(list.clone()) {
      match partial_sums.get_mut(&factor) {
        Some(sum) => *sum += val,
        None => { partial_sums.insert(factor, val); }
      }
    }
    output.push(last_digit(partial_sums.iter().map(|(&factor, &total)| factor*total as i32).sum()));
  }
  output
}

// TODO: Not sure about this "mut list" business
fn fft_loop(mut list: Vec<u32>, factors: &Vec<Vec<i32>>, phases: u32) -> Vec<u32> {
  let mut phase_count = 0;
  loop {
    list = fft(&list.iter(), &factors.iter());
    phase_count += 1;
    if phase_count == phases { break list }
  }
}

#[cfg(test)]
mod test {
  use super::*;

  fn elementwise_add(mut lists: Vec<Vec<i32>>) -> Vec<i32> {
    let mut total = lists.pop().unwrap();
    for l in lists.iter() {
      assert!(l.len() == total.len());
      total.iter_mut().zip(l).map(|(a, &b)| *a += b).count();
    }
    total
  }

  #[test]
  fn base_iter() {
    assert_eq!(
      vec![1,0,-1,0,1],
      BasePattern::new(1).take(5).collect::<Vec<_>>());

    assert_eq!(
      vec![0,1,1,0,0,-1,-1,0,0,1,1,0],
      BasePattern::new(2).take(12).collect::<Vec<_>>());

    assert_eq!(
      vec![0,0,1,1,1,0,0,0,-1,-1,-1,0,0,0,1,1,1,0,0,0,-1],
      BasePattern::new(3).take(21).collect::<Vec<_>>());
  }

  #[test]
  fn examples() {
    let input: Vec<u32> = vec![1,2,3,4,5,6,7,8];
    let factors = build_factors(input.len(), 1);

    let input = fft(&input.iter(), &factors.iter());
    assert_eq!(vec![4,8,2,2,6,1,5,8], input);

    let input = fft(&input.iter(), &factors.iter());
    assert_eq!(vec![3,4,0,4,0,4,3,8], input);

    let input = fft(&input.iter(), &factors.iter());
    assert_eq!(vec![0,3,4,1,5,5,1,8], input);
  }

  fn str_loop(list: &str, factors: &Vec<Vec<i32>>, phases: u32) -> String {
    str_from_arr(&fft_loop(arr_from_str(list), factors, phases))
  }

  #[test]
  fn big_examples() {
    let factors = build_factors(32, 1);
    assert_eq!("24176176", &str_loop("80871224585914546619083218645595", &factors, 100)[0..8]);
    assert_eq!("73745418", &str_loop("19617804207202209144916044189917", &factors, 100)[0..8]);
    assert_eq!("52432133", &str_loop("69317163492948606335995924319873", &factors, 100)[0..8]);
  }

  #[test]
  fn repeated_list() {
    assert_eq!(vec![1,2,3,1,2,3,1,2,3], RepeatedList::from_vec(&vec![1,2,3],3).collect::<Vec<_>>());
  }

  #[test]
  fn factor_table() {
    let mut desired_factors = vec![
      elementwise_add(vec![
        vec![1,0,-1,0,1],
        vec![0,-1,0,1,0],
        vec![-1,0,1,0,-1],
      ]),
      elementwise_add(vec![
        vec![0,1,1,0,0],
        vec![-1,-1,0,0,1],
        vec![1,0,0,-1,-1],
      ]),
      elementwise_add(vec![
        vec![0,0,1,1,1,],
        vec![0,0,0,-1,-1],
        vec![-1,0,0,0,1],
      ]),
      elementwise_add(vec![
        vec![0,0,0,1,1],
        vec![1,1,0,0,0],
        vec![0,-1,-1,-1,-1],
      ]),
      elementwise_add(vec![
        vec![0,0,0,0,1,],
        vec![1,1,1,1,0],
        vec![0,0,0,0,-1],
      ]),
    ];
    // Generate the rest, I'm tired of typing
    for i in 6..=15 {
      desired_factors.push(
        elementwise_add(
          BasePattern::new(i).take(15).collect::<Vec<_>>()
            .chunks(5).take(3).map(|c| c.iter().map(|c| *c).collect()).collect()
        )
      );
    }
    assert_eq!(desired_factors, build_factors(5, 3))
  }
}
