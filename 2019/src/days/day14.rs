use crate::util::first_two;
use std::collections::HashMap;

type Quant = (u64, String);
type QuantRef<'a> = (u64, &'a str);

#[derive(Eq, PartialEq, Debug)]
struct Recipe {
  input: Vec<Quant>,
  output: Quant,
}
impl Recipe {
  fn new(input: Vec<Quant>, output: Quant) -> Recipe {
    Recipe{input, output}
  }
  fn from_ref(input: Vec<QuantRef>, output: QuantRef) -> Recipe {
    Recipe {
      input: input.iter().map(|q| (q.0, String::from(q.1))).collect(),
      output: (output.0, String::from(output.1))
    }
  }
}

struct Spec {
  products: HashMap<String, (u64, Vec<Quant>)>,
}
impl Spec {
  fn from_recipes(recipes: Vec<Recipe>) -> Spec {
    let mut spec = Spec{products: HashMap::new()};
    for r in recipes {
      spec.products.insert(r.output.1, (r.output.0, r.input));
    }
    spec
  }

  // I'm not sure how this works without avoiding
  // counting A forever but I'm happy with it
  // TODO: Use cache
  fn dist(&self, product: &str) -> u64 {
    let (_, components) = &self.products[product];
    components.iter().map(|(_, c)| {
      match c {
        c if c == "ORE" => 0,
        _ => self.dist(c)
      }
    }).max().unwrap() + 1
  }

  fn cost(&self, qty: u64, product: &str) -> u64 { self.cost_impl(qty, product, 0) }
  fn cost_impl(&self, qty: u64, product: &str, depth: usize) -> u64 {
    let mut decomposed: HashMap<String, u64> = HashMap::new();
    decomposed.insert(String::from(product), qty);
    loop {
      let max_dist = decomposed.keys().map(|p| self.dist(p)).max().unwrap();
      let (this_round, mut remainder): (HashMap<String,u64>, HashMap<String,u64>) = decomposed.drain().partition(|(p, _)| self.dist(p) == max_dist);
      for (p, qty) in this_round {
        let (makes, components) = &self.products[&p];
        let to_make = (qty+makes-1)/makes;
        for (count, component) in components {
          let needed = to_make*count;

          match remainder.get_mut(component) {
            Some(qty) => *qty += needed,
            None => {remainder.insert(String::from(component), needed);}
          }
        }
      }
      decomposed = remainder;
      if decomposed.keys().collect::<Vec<_>>() == vec!["ORE"] { break }
    }
    decomposed["ORE"]
  }

  fn max_fuel(&self, with_ore: u64) -> u64 {
    let mut low = with_ore/self.cost(1, "FUEL");
    let mut high = low*2;
    loop {
      debug!("Checking between {} and {}...", low, high);
      debug!("{} {}",self.cost(low, "FUEL"), self.cost(high, "FUEL"));
      let mid = (low+high)/2;
      if low == mid { return low; }
      if self.cost(mid, "FUEL") <= with_ore {
        low = mid;
      } else {
        high = mid;
      }
    }
  }
}

pub fn run(input: String, part: Option<u8>) -> String {
  let spec = Spec::from_recipes(parse_recipes(&input));


  match part {
    Some(1) => spec.cost(1, "FUEL"),
    Some(2) | None => spec.max_fuel(1_000_000_000_000),
    Some(p) => panic!("Unknown part {}!", p)
  }.to_string()
}

fn parse_recipes(input: &str) -> Vec<Recipe> {
  let mut spec = Vec::new();
  let parse_quant = |s: &str| -> (u64, String) {
    let out = first_two(s.split(' '));
    (out.0.parse::<u64>().unwrap(), String::from(out.1))
  };

  for l in input.lines() {
    let (input, output) = first_two(l.split(" => "));
    let input = input.split(", ").map(parse_quant).collect();
    let output = parse_quant(output);
    spec.push(Recipe::new(input, output));
  }
  spec
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn ex1() {
    let spec = parse_recipes(&example1);
    assert_eq!(vec![
      Recipe::from_ref(
        vec![(10, "ORE")],
        (10, "A")
      ),
      Recipe::from_ref(
        vec![(1, "ORE")],
        (1, "B")
      ),
      Recipe::from_ref(
        vec![(7, "A"), (1, "B")],
        (1, "C")
      ),
      Recipe::from_ref(
        vec![(7, "A"), (1, "C")],
        (1, "D")
      ),
      Recipe::from_ref(
        vec![(7, "A"), (1, "D")],
        (1, "E")
      ),
      Recipe::from_ref(
        vec![(7, "A"), (1, "E")],
        (1, "FUEL")
      ),
    ], spec);

    let spec = Spec::from_recipes(spec);
    assert_eq!(5, spec.dist("FUEL"));
    assert_eq!(4, spec.dist("E"));
    assert_eq!(3, spec.dist("D"));
    assert_eq!(2, spec.dist("C"));
    assert_eq!(1, spec.dist("B"));
    assert_eq!(1, spec.dist("A"));

    assert_eq!(31, spec.cost(1, "FUEL"));
  }

  #[test]
  fn large() {
    let spec = Spec::from_recipes(parse_recipes(&large1));
    assert_eq!(13312, spec.cost(1, "FUEL"));
    assert_eq!(82892753, spec.max_fuel(1_000_000_000_000));

    let spec = Spec::from_recipes(parse_recipes(&large2));
    assert_eq!(180697, spec.cost(1, "FUEL"));
    assert_eq!(5586022, spec.max_fuel(1_000_000_000_000));

    let spec = Spec::from_recipes(parse_recipes(&large3));
    assert_eq!(2210736, spec.cost(1, "FUEL"));
    assert_eq!(460664, spec.max_fuel(1_000_000_000_000));
  }

  const example1: &str =
"10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL";

  const large1: &str =
"157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT";

  const large2: &str =
"2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF";

  const large3: &str =
"171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX";
}
