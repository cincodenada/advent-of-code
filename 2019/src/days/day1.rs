fn fuel_cost(mass : u32) -> u32 {
   return match ((mass as f32)/3.0).floor() as i32 - 2 {
     x if x <= 0 => 0,
     x => x as u32
   }
}

fn total_cost(masses: Vec<u32>, count_fuel: bool) -> u32 {
  let mut total = 0;
  for mass in masses {
    let mut fuel = fuel_cost(mass);
    total += fuel;
    if count_fuel {
      while fuel > 0 {
        fuel = fuel_cost(fuel);
        total += fuel;
      }
    }
  }
  total
}

pub fn run(input: String, part: Option<u8>) -> String {
  let lines = input.lines().into_iter().map(|s| s.parse::<u32>().unwrap()).collect();
  total_cost(lines, part == Some(2)).to_string()
}
