use crate::util::intcode;

pub fn run(input: String, part: Option<u8>) -> String {
  let ints : Vec<i128> = input.trim().split(',').map(|s| s.parse::<i128>().unwrap()).collect();
  let input = match part {
    Some(1) => vec![1],
    Some(2) | None => vec![5],
    Some(d) => panic!("Invalid part {}", d)
  };

  let mut prog = intcode::Program::from_vec_input(ints, input);
  prog.run();

  (*prog.output.last().unwrap()).to_string()
}
