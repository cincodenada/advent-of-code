use crate::util::intcode;

pub fn run(input: String, part: Option<u8>) -> String {
  let mut prog = intcode::Program::from_csv(input.trim());
  prog.input.push(part.unwrap() as i128);
  prog.run();

  prog.output[0].to_string()
}
