pub fn run(input: String, part: Option<u8>) -> String {
  match part {
    Some(1) => String::new(),
    Some(2) | None => String::new(),
    Some(p) => panic!("Unknown part {}!", p)
  }
}

#[cfg(test)]
mod test {
  use super::*;
}
