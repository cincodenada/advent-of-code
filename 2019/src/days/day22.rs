use crate::util;

pub fn run(input: String, part: Option<u8>) -> String {
  let (len, mut idx, reps) = match part {
    Some(1) => (10007, 2019, 1),
    _ => (119315717514047_usize, 2020, 101741582076661_usize),
  };

  let instructions = input.lines().map(|l| {
    let (param, mut op) = util::first_two(l.rsplitn(2, " "));
    let param = match param {
      "stack" => { op = l; 0 },
      snum => param.parse().unwrap(),
    };
    (op, param)
  }).collect::<Vec<_>>();

  let orig_idx = idx;
  let mut runs_to_repeat = None;
  for i in 0..reps {
    for (op, param) in instructions.iter() {
      idx = new_pos(idx, len, op, *param);
    }
    if idx == orig_idx { runs_to_repeat = Some(i); break }
  }

  println!("Repeated after {:?} runs", runs_to_repeat);

  if runs_to_repeat != None {
    for _ in 0..(reps % runs_to_repeat.unwrap()) {
      for (op, param) in instructions.iter() {
        idx = new_pos(idx, len, op, *param);
      }
    }
  }

  idx.to_string()
}

fn new_pos(idx: usize, len: usize, method: &str, param: i64) -> usize {
  let sidx = idx as i64;
  let slen = len as i64;

  debug!("Executing {} with {}", method, param);

  let new_idx = match method {
    "cut" => sidx - param,
    "deal with increment" => sidx*param,
    "deal into new stack" => (slen-1)-sidx,
    other => panic!("Unknown method {}", other),
  };
  ((new_idx + slen) % slen) as usize
}

fn materialize<T>(it: T) -> Vec<usize>
where T: ExactSizeIterator<Item=usize> {
  let mut v = Vec::with_capacity(it.len());
  v.resize(it.len(), 0);
  for (val, pos) in it.enumerate() { v[pos] = val }
  v
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn examples() {
    let cards = 0..10;

    assert_eq!(vec![9,8,7,6,5,4,3,2,1,0], materialize(
      cards.clone().map(|idx| new_pos(idx, cards.len(), "deal into new stack", 0))
    ));
    assert_eq!(vec![3,4,5,6,7,8,9,0,1,2], materialize(
      cards.clone().map(|idx| new_pos(idx, cards.len(), "cut", 3))
    ));
    assert_eq!(vec![6,7,8,9,0,1,2,3,4,5], materialize(
      cards.clone().map(|idx| new_pos(idx, cards.len(), "cut", -4))
    ));
    assert_eq!(vec![0,7,4,1,8,5,2,9,6,3], materialize(
      cards.clone().map(|idx| new_pos(idx, cards.len(), "deal with increment", 3))
    ));
  }

  #[test]
  fn shuffle_examples() {
    let cards = 0..10;


    assert_eq!(vec![0,3,6,9,2,5,8,1,4,7], materialize(
      cards.clone()
        .map(|idx| new_pos(idx, cards.len(), "deal with increment", 7))
        .map(|idx| new_pos(idx, cards.len(), "deal into new stack", 0))
        .map(|idx| new_pos(idx, cards.len(), "deal into new stack", 0))
    ));

    assert_eq!(vec![3,0,7,4,1,8,5,2,9,6], materialize(
      cards.clone()
        .map(|idx| new_pos(idx, cards.len(), "cut", 6))
        .map(|idx| new_pos(idx, cards.len(), "deal with increment", 7))
        .map(|idx| new_pos(idx, cards.len(), "deal into new stack",0))
    ));

    assert_eq!(vec![6,3,0,7,4,1,8,5,2,9], materialize(
      cards.clone()
        .map(|idx| new_pos(idx, cards.len(), "deal with increment", 7))
        .map(|idx| new_pos(idx, cards.len(), "deal with increment", 9))
        .map(|idx| new_pos(idx, cards.len(), "cut",-2))
    ));

    assert_eq!(vec![9,2,5,8,1,4,7,0,3,6], materialize(
      cards.clone()
        .map(|idx| new_pos(idx, cards.len(), "deal into new stack",0))
        .map(|idx| new_pos(idx, cards.len(), "cut",-2))
        .map(|idx| new_pos(idx, cards.len(), "deal with increment",7))
        .map(|idx| new_pos(idx, cards.len(), "cut", 8))
        .map(|idx| new_pos(idx, cards.len(), "cut",-4))
        .map(|idx| new_pos(idx, cards.len(), "deal with increment",7))
        .map(|idx| new_pos(idx, cards.len(), "cut", 3))
        .map(|idx| new_pos(idx, cards.len(), "deal with increment",9))
        .map(|idx| new_pos(idx, cards.len(), "deal with increment",3))
        .map(|idx| new_pos(idx, cards.len(), "cut",-1))
    ));
      //deal_new(deal_new(deal_increment(cards.clone(), 7))).collect::<Vec<u32>>());
  }
}
