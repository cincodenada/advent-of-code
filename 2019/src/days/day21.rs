use crate::util::intcode;
use std::io;
use std::io::prelude::*;

struct SpringDroid {
  prog: intcode::Program,
}
impl SpringDroid {
  fn from_csv(prog: &str) -> SpringDroid {
    SpringDroid {
      prog: intcode::Program::from_csv(prog)
    }
  }

  fn run(&mut self, prog: &str) -> Result<(i128, String), String> {
    self.prog.run();

    while self.prog.get() != None {}

    for c in prog.chars() {
      self.prog.put(c as i128);
    }

    self.prog.resume();
    self.output()
  }

  fn output(&mut self) -> Result<(i128, String), String> {
    let mut out = String::new();
    loop {
      match self.prog.get() {
        Some(n) if n > 255 => return Ok((n, out)),
        Some(c) => out.push(c as u8 as char),
        None => break
      }
    }
    Err(out)
  }

  fn console(&mut self) -> i128 {
    loop {
      self.prog.run();

      let mut prompt = Vec::new();
      while match self.prog.get() {
        Some(v) => {prompt.push(v as u8); true}
        None => false
      } {}
      io::stdout().write_all(&prompt);

      let mut command = String::new();
      while command != "WALK\n" {
        command.clear();
        io::stdin().read_line(&mut command);
        for c in command.chars() {
          self.prog.put(c as i128);
        }
      }
      self.prog.resume();

      match self.output() {
        Ok((damage, msg)) => {
          io::stdout().write_all(msg.as_bytes());
          return damage
        },
        Err(err) => println!("{}", err),
      }
    };
  }
}

struct SpringScript<'a> {
  prog: std::str::Lines<'a>,
  j: bool,
  t: bool,
  floor: &'a str,
}
impl<'a> SpringScript<'a> {
  fn from_str(prog: &'a str) -> SpringScript {
    SpringScript {
      prog: prog.lines(),
      j: false,
      t: false,
      floor: "",
    }
  }

  fn run(&mut self, floor: &'a str) -> bool {
    self.j = false;
    self.t = false;
    self.floor = floor;
    println!("{}\n{}", floor, "ABCDEFGHI");
    for l in self.prog.clone() {
      let mut parts = l.split(' ');
      let opcode = parts.next();
      let ra = parts.next();
      let rb = parts.next();

      match opcode {
        Some("NOT") => self.set(rb, !self.get(ra)),
        Some("AND") => self.set(rb, self.get(ra) && self.get(rb)),
        Some("OR") => self.set(rb, self.get(ra) || self.get(rb)),
        Some("WALK") => (),
        Some("RUN") => (),
        Some(badop) => panic!("Bad opcode {}", badop),
        None => panic!("Missing opcode"),
      }

      println!("{} {} {} -> T:{} J:{}",
        opcode.unwrap_or("???"), ra.unwrap_or("?"), rb.unwrap_or("?"),
        self.t, self.j);
    }
    self.j
  }

  fn get(&self, reg: Option<&str>) -> bool {
    match reg.expect("Missing register!").chars().next() {
      Some('T') => self.t,
      Some('J') => self.j,
      Some(c @ 'A'..='I') => {
        let idx = (c as u8 - 'A' as u8) as usize;
        self.floor.chars().nth(idx) == Some('#')
      },
      Some(c) => panic!("Invalid register {}", c),
      None => panic!("No register specified!"),
    }
  }

  fn set(&mut self, reg: Option<&str>, val: bool) {
    match reg.expect("Missing register!").chars().next() {
      Some('T') => self.t = val,
      Some('J') => self.j = val,
      Some(c) => panic!("Invalid register {}", c),
      None => panic!("No register specified!"),
    }
  }
}

const PROG1: &str =
"NOT C T
OR T J
NOT B T
OR T J
NOT A T
OR T J
AND D J
WALK\n";

/*
 *
.................
.................
..@..............
#####.#.##.######
   ABCDEFGHI
    *ABCDEFGHI
 *
.................
.................
....@............
#####...###...###
     ABCDEFGHI

J && (H || (E && I))
J && (H || !(!E || !I))
J && !(!H && (!E || !I))
*/

const PROG2: &str =
"NOT C T
OR T J
NOT B T
OR T J
NOT A T
OR T J
AND D J
AND H J
NOT A T
OR T J
RUN\n";

pub fn run(input: String, part: Option<u8>) -> String {
  let mut frog = SpringDroid::from_csv(&input);

  let damage = match part {
    Some(1) => frog.run(PROG1),
    Some(2) => frog.run(PROG2),
    _ => Ok((frog.console(), String::from("Done"))),
  };

  match damage {
    Ok((damage, msg)) => damage.to_string(),
    Err(err) => annotate(err)
  }
}

fn annotate(death: String) -> String {
  let mut out = String::new();

  let mut line_start = 0;
  let mut bot_pos = 0;
  let mut found_floor = false;
  for (idx, c) in death.chars().enumerate() {
    out.push(c);
    match c {
      '\n' => {
        line_start = idx + 1;
        if found_floor {
          let add_line = format!("{}{}\n", " ".repeat(bot_pos), "ABCDEFGHI");
          out.push_str(&add_line);
          line_start = add_line.len();
          found_floor = false;
        }
      },
      '@' => bot_pos = idx - line_start+1,
      '#' => found_floor = true,
      _ => ()
    }
  }
  out
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn spring_script() {
    let mut naive = SpringScript::from_str(PROG1);

    assert_eq!(false, naive.run("###   ###"));
    assert_eq!(true, naive.run("   ######"));
    assert_eq!(true, naive.run("##.#.##.#"));

    let mut smarter = SpringScript::from_str(PROG2);

    //                            ABCDEFGHI
    assert_eq!(true, smarter.run(".#.##.#####"));
    assert_eq!(true, smarter.run("...###...###"));
  }
}
