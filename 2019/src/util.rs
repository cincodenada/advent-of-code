pub mod intcode;
pub mod tilemap;
pub mod geom;

use std::cmp;
use std::collections::HashMap;
use geom::Vec2; // For draw_map, get rid of this

pub fn cls() -> String {
  format!("{}[2J", 27 as char)
}

pub fn delay(millis: u64) {
  std::thread::sleep(std::time::Duration::from_millis(millis));
}

pub fn first_two<T, I>(mut it: I) -> (T, T)
where I: Iterator<Item = T> {
  (it.next().unwrap(), it.next().unwrap())
}
pub fn first_three<T, I>(mut it: I) -> (T, T, T)
where I: Iterator<Item = T> {
  (it.next().unwrap(), it.next().unwrap(), it.next().unwrap())
}

pub fn draw_map<T, F>(map: &HashMap<Vec2, T>, charsel: F, overlays: Vec<(Vec2, char)>) -> String
  where F: Fn(Option<&T>) -> char
{
  let mut max = Vec2::from_xy(std::i32::MIN, std::i32::MIN);
  let mut min = Vec2::from_xy(std::i32::MAX, std::i32::MAX);
  for p in map.keys() {
    max.x = cmp::max(max.x, p.x);
    min.x = cmp::min(min.x, p.x);
    max.y = cmp::max(max.y, p.y);
    min.y = cmp::min(min.y, p.y);
  }

  let mut overlay_map = HashMap::new();
  for o in overlays {
    overlay_map.insert(o.0, o.1);
  }

  let mut out = String::new();
  for y in (min.y..=max.y).rev() {
    for x in min.x..=max.x {
      let loc = Vec2::from_xy(x, y);
      out.push(match overlay_map.get(&loc) {
        Some(&c) => c,
        None => charsel(map.get(&loc))
      });
    }
    out.push('|');
    out.push_str(&y.to_string());
    out.push('\n');
  }
  let xwidth = (max.x-min.x+1) as usize;
  out.push_str(&"-".repeat(xwidth));
  out.push('\n');
  out.push_str(&format!("{:<wl$}{:>wr$}", min.x, max.x, wl=xwidth/2, wr=(xwidth+1)/2));
  out
}


pub mod nums {
  use primes::PrimeSet;
  pub fn lcm(mut nums: Vec<u64>) -> u64 {
    let mut primes = PrimeSet::new();
    let mut lcm = 1;
    for p in primes.iter() {
      let count = nums.iter_mut().map(|n| {
        let mut count = 0;
        while *n % p == 0 { *n /= p; count+=1; }
        count
      }).max();
      lcm *= p.pow(count.unwrap());

      if count == Some(0) {
        if nums.iter().filter(|n| **n > p).collect::<Vec<_>>().len() == 0 {
          break;
        }
      }
    }
    lcm
  }

  pub fn permutations<T>(init: Vec<T>) -> Vec<Vec<T>>
  where T: Clone + Copy {
    if init.len() == 1 {
      return vec![init]
    }

    let mut combos: Vec<Vec<T>> = Vec::new();
    for (idx, el) in init.iter().enumerate() {
      let mut remaining = init.clone();
      remaining.remove(idx);
      for suffix in permutations(remaining) {
        combos.push({let mut v = suffix.clone(); v.push(*el); v});
      }
    }
    combos
  }

  // This is horribly inefficient, but functional
  pub fn choose_n<T>(init: Vec<T>, n: usize) -> Vec<Vec<T>>
  where T: Clone + Copy + Default + Ord {
    let mut combos = permutations(init.clone());
    for c in &mut combos { c.resize(n, T::default()); }
    combos.sort();
    combos.dedup();
    combos
  }

  #[cfg(test)]
  mod test {
    use super::*;

    #[test]
    fn test_lcm() {
      assert_eq!(12, lcm(vec![3, 4, 6]));
      assert_eq!(42, lcm(vec![7, 6]));
      assert_eq!(1*3*5*7, lcm(vec![1,3,5,7]));
      assert_eq!(60, lcm(vec![20,30]));
    }

    #[test]
    fn test_permutations() {
      assert_eq!(vec![vec![1,0],vec![0,1]], permutations(vec![0,1]));

      assert_eq!(vec![
        vec![3,2,1],vec![2,3,1],
        vec![3,1,2],vec![1,3,2],
        vec![2,1,3],vec![1,2,3],
      ], permutations(vec![1,2,3]));
    }

    #[test]
    fn test_choose_n() {
      assert_eq!(vec![vec![0],vec![1]], choose_n(vec![0,1],1));
      assert_eq!(vec![
        vec![1,2],vec![1,3],
        vec![2,1],vec![2,3],
        vec![3,1],vec![3,2],
      ], choose_n(vec![1,2,3], 2));
      assert_eq!(vec![
        vec![1],vec![2],vec![3],
      ], choose_n(vec![1,2,3], 1));
    }
  }
}

