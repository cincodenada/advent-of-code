#[macro_use]
extern crate log;

mod days;
mod util;

use std::env;
use std::io::Write;

enum RunMode {
  RUN,
  DUMP
}

fn main() {
  env_logger::builder()
    .format(|buf, record| {
        writeln!(buf, "{}: {}", record.level(), record.args())
    })
    .init();

  let mut mode = RunMode::RUN;
  let mut args: Vec<String> = env::args().collect();
  if args[1] == "dump" {
    mode = RunMode::DUMP;
    args.remove(1);
  }

  let args: Vec<u8> = args[1..].iter().map(|s| s.parse::<u8>().unwrap()).collect();
  let (day, part) = match args.len() {
    0 => (None, None),
    1 => (Some(args[0]), None),
    2 => (Some(args[0]), Some(args[1])),
    _ => panic!("Too many arguments!")
  };

  match mode {
    RunMode::RUN => days::run(day, part),
    RunMode::DUMP => {
      let prog = util::intcode::Program::from_csv(&days::get_input(day.expect("Must specify day!")));
      prog.dump();
    }
  };
}
