~~~

'5483143223 s:keep
'2745854711 s:keep
'5264556173 s:keep
'6141336146 s:keep
'6357385478 s:keep
'4167524645 s:keep
'2176841721 s:keep
'6882881134 s:keep
'4846848554 s:keep
'5283751526 s:keep

'util.forth include

(ayx-n)
:sq:ref [ a:th fetch ] dip a:th ;
:sq:get sq:ref fetch ;
(aq-)
:sq:for-each swap [ over a:for-each ] a:for-each drop ;
(aqq-)
:sq:rowcol 
  swap rot 
  [
    over a:for-each
    over call
  ] a:for-each
  drop-pair
;


 { #-1 #0 #1 } 'rel-neighbors var-n
(q-)
:sq:neighbors
  'enter dl
  'func var
  @func n:put
  'neighbor-base var
   !func
  @func n:put
  @rel-neighbors [
    !neighbor-base
    @rel-neighbors [
      @neighbor-base swap
      'inner dl
      @func n:put
      dup-pair [ #0 eq? ] bi@ and not 
        'call dl
        @func [ drop-pair ] choose
    ] a:for-each
    drop
  ] a:for-each
  drop
;
(ayxq-)
:sq:neighbor-refs
  's dl
  'outerfunc var-n
  'relx var-n
  'rely var-n
  'arr var-n
  [
    'il dl
    [ @rely + ] [ @relx + ] bi*
    'nl dl
    @arr sq:ref @outerfunc call
  ] sq:neighbors
  drop
;

'linecount var
'octopi var
#0 !linecount
[ 
  [
    s:to-array [ c:to-number ] a:map
    @linecount n:inc !linecount
  ] runstack 
  @linecount
] a:make !octopi

'width var
'height var
@octopi a:length !height
@octopi #0 a:th fetch a:length !width

@height [
  I
  @width [
    dup @octopi swap I
    sq:get n:put sp
  ] indexed-times
  drop nl
] indexed-times

@octopi [ n:put ] sq:for-each
@octopi [ n:put ] [ nl ] sq:rowcol
@octopi #2 #2 [ fetch n:put nl ] sq:neighbor-refs
