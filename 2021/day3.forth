Each bit in the gamma rate can be determined by finding the most common bit in the corresponding position of all numbers in the diagnostic report. For example, given the following diagnostic report:

#0
'00100
'11110
'10110
'10111
'10101
'01111
'00111
'11100
'10000
'11001
'00010
'01010

gamma: 10110 / epsilon: 01001 

~~~
'sum var
#0 !sum
{ #0 #0 #0 #0 #0 #0 #0 #0 #0 #0 #0 #0 } !sum

:dumparr $[ c:put sp [ n:put sp ] a:for-each $] c:put nl ;

:runfile 'day3.dat swap file:for-each-line ;

'day3.dat [
  dup s:put nl
  @sum #0 eq? [
    dup s:put nl
    dup a:from-string [ drop #0 ] a:map !sum
  ] if
  [ c:to-number #-1 shift #1 swap - ] s:for-each
  @sum [ + ] a:map !sum
] file:for-each-line

@sum dumparr
#1 @sum [
  #0 gt? [ #0 ] [ dup ] choose
  [ #-1 shift ] dip
] a:map nip
#0 [ + ] a:reduce

#1 @sum [
  #0 gt? [ dup ] [ #0 ] choose
  [ #-1 shift ] dip
] a:map nip
#0 [ + ] a:reduce

*

n:put

~~~

