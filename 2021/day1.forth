# Day 1: Sonar Sweep

## Part 1

First, put the input in a file called input1 and that test case a file called test1 (this is the pattern I am going to use throughout).
The solution is pretty simple: create variables to store the previous number and the count. Then loop through the file. If the current number is more than the previous, add one to the count. Then, store the current number in the variable from the previous one.

Prev is defined as being 9999, larger than any of the values in the file, so the first entry will always not be counted as higher (to avoid needing extra logic for the first entry). We also count the amount of lines the the file, to use in part 2.

~~~
'count var
'lines var
#9999 'prev var-n
~~~

then loop through the file counting up. Retro has a nice word, for-each-line, that does the looping. 

~~~
'day1.dat [ s:to-number dup @prev gt? [ &count v:inc ] if !prev &lines v:inc ] file:for-each-line

@count n:put ASCII:LF c:put
~~~

## Part 2

The above is all from @nihilazo@tiny.tilde.town - the below is my adapdation of
their solution, but it ended up mostly just being a rewrite because I just
ported my QB64 solution over, more or less.

It's got a bunch of debug to show what's going on that I left in, and compare
to the QB64 solution if you find that easier to read, but basically:

push 0 to the stack, this is where we'll count the changes

keep a rolling list of the last three numbers:
 - put input number in the list at `idx`
 - Update `idx = idx + 1 % 3` (-> 0 1 2 0 1 2 0 1 2...)

if we're at or past the third line, we can start checking:
 - sum up the array
 - if it's > prev, increment the counter
 - store the new total in prev

And at the end, output the counter. Yay!

Vars
~~~
#99999 !prev
'window var
'idx var
'lineno var
{ #0 #0 #0 } !window
~~~

Debug methods
~~~
:nl ASCII:LF c:put ;
:n:show n:put nl ;
:n:peek dup n:show ;
:n:peek2 over n:show ;
:n:peek3 rot rot dup n:show rot ;
:s:show s:put nl ;
:n:label swap s:put n:show ;
:n:labelp s:put n:peek ;
:dbg-stack 'stack:_ s:put dump-stack nl ;
~~~

Check Bigger - this is the meat of the logic!

This function expects the following on the stack, from top to bottom:
line number
(pointer to) array of last three items
previous sum to compare to
total number of increases

Outputs 
increments the total number, if relevant, and leaves the total of the array on the stack

Note: we actually overwrite prev before comparing, because at that point the
previous sum is already duplicated on the stack as an input parameter and thus
safe

The `dup rot` is because at that point the stack is:
count prev total <- Top
And we want to end up with the total just above count, so we:
dup -> count prev total total <- Top
rot -> count total total prev <- Top
Then we can consume prev/total, and have total left over

~~~
(nbab-mo)
:check-bigger
  dbg-stack
  #2 gt? [
   #0 [ + ] a:reduce
   'total:_ s:put n:peek
   dup rot
   'prev:_ s:put n:peek
   gt? [ swap #1 + swap ] if
  ] [
    drop
  ] choose
;
~~~
And the reading...
~~~
#0 (count)
'day1.tst [
  s:to-number
  @lineno n:inc !lineno
  'line:_ @lineno n:label
  'reading:_ n:labelp

  (put_reading_in_window)
  @window @idx a:th store 

  (are_we_bigger?)
  @prev @window @lineno check-bigger !prev

  (rotate_window_pos)
  @idx #1 + #3 mod !idx
] file:for-each-line
n:put nl
~~~

Alternate solution without arrays, just using rotations:

This takes advantage of the fact that we can reach up to 3 deep with rot, which
is just the right amount!

We build up until we have at least four soundings:

1 2 3 4 <- Top

Then we use the address stack to make a copy and sum up each group, discarding
the first item once we're done, so we don't grow the stack infinitely.
We use a custom word sum-three to sum the top three items of the stack:

(start)   | 2 3 4   | ...
push push | 2       | ...4 3
pop dup   | 2 3 3   | ...4
rot +     | 3 5     | ...4
pop dup   | 3 5 4 4 | ...
rot +     | 3 4 9   | ...

(I think this could be extended to arbitrary lengths, and I'm sure I'm just
rediscovering a well-established algorithm here - with a pop after the initial
push push, this non-destructively sums the last three items, which was what I
originally had)

~~~
'count var
#0 !count
#9999 !prev

:sum-three
  push push
  pop dup rot +
  pop dup rot +
;

'day1.dat [
  s:to-number
  depth #2 gt? [
    sum-three dup
    @prev gt? [ @count n:inc !count ] if
    !prev
  ] if 
] file:for-each-line
@count n:put
