OPEN "day1.dat" FOR INPUT AS #1
DIM readings(3) AS INTEGER
idx = 0
have_three = FALSE
prev = 0
increased = 0
total = -1
DO UNTIL EOF(1)
    INPUT #1, m
    readings(idx) = m
    idx = (idx + 1) MOD 3
    IF idx = 0 THEN
        have_three = TRUE
    END IF
    IF have_three THEN
        total = sum(readings)
    END IF
    IF prev > -1 AND total > prev THEN
        increased = increased + 1
    END IF
    prev = total
LOOP
PRINT increased

FUNCTION sum (arr)
    total = 0
    FOR i = 0 TO UBOUND(arr)
        total = total + ARR(i)
    NEXT
    sum = total
END FUNCTION
