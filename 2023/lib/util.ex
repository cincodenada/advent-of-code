defmodule Util do
  defprotocol Day do
    @doc "Code for a specific day of Advent of Code"

    def part1(input)

    def part2(input)
  end

  def get_numbers(string) do
    string
    |> String.codepoints()
    |> Enum.filter(&(&1 >= "0" and &1 <= "9"))
    |> Enum.map(&String.to_integer/1)
  end

  def get_spelled_numbers(string) do
    case string do
      "one" <> rest ->
        [1] ++ get_spelled_numbers("ne" <> rest)

      "two" <> rest ->
        [2] ++ get_spelled_numbers("wo" <> rest)

      "three" <> rest ->
        [3] ++ get_spelled_numbers("ree" <> rest)

      "four" <> rest ->
        [4] ++ get_spelled_numbers("our" <> rest)

      "five" <> rest ->
        [5] ++ get_spelled_numbers("ive" <> rest)

      "six" <> rest ->
        [6] ++ get_spelled_numbers("ix" <> rest)

      "seven" <> rest ->
        [7] ++ get_spelled_numbers("even" <> rest)

      "eight" <> rest ->
        [8] ++ get_spelled_numbers("ight" <> rest)

      "nine" <> rest ->
        [9] ++ get_spelled_numbers("ine" <> rest)

      <<c::binary-size(1)>> <> rest when c >= "0" and c <= "9" ->
        [String.to_integer(c)] ++ get_spelled_numbers(rest)

      <<_::binary-size(1)>> <> rest ->
        get_spelled_numbers(rest)

      "" ->
        []
    end
  end

  def tee(var) do
    IO.inspect(var)
    var
  end

  def parse(spec, target) when is_list(spec) do
    Enum.map(spec, &target.parse/1)
  end

  def parse(spec, target) do
    target.parse(spec)
  end

  def parse(spec, target, split: sep) do
    parse(spec |> String.split(sep, trim: true), target)
  end
end

defmodule Util.Macros do
  def strip_body({:def, info, [args, _body]}) do
    {:def, info, [args]}
  end

  def strip_body({:__block__, [], deflist}) do
    {:__block__, [], Enum.map(deflist, &strip_body/1)}
  end

  defmacro run(daypart, body) do
    "d" <> dp = Atom.to_string(elem(daypart, 0))
    [day, part] = String.split(dp, "p")

    ast_day = {:__aliases__, [alias: false], [String.to_existing_atom("Day#{day}")]}
    atom_part = String.to_existing_atom("part#{part}")

    Enum.map(body[:do], fn {:->, _, [[input], expected]} ->
      input_name = Macro.to_string(input)

      quote do
        {uSecs, ans} =
          :timer.tc(unquote(ast_day), unquote(atom_part), [
            unquote(input)
          ])

        assert ans == unquote(expected)

        IO.puts(
          "Ran against #{unquote(input_name)} in #{uSecs} us"
        )
      end
    end)

    # quote do

    #  IO.puts(
    #    "Day #{unquote(num)} part #{unquote(part)} ran #{unquote(input_name)} in #{uSecs} us"
    #  )
    # end
  end

  defmacro defprotoimpl(name, body) do
    quote do
      defprotocol unquote(name) do
        @fallback_to_any true
        unquote(body[:do] |> strip_body)
      end

      defimpl unquote(name), for: Any do
        unquote(body)
      end
    end
  end
end

defmodule Util.Parser do
  import NimbleParsec

  ws = ignore(repeat(string(" ")))
  num = integer(min: 1)
  nums = wrap(times(ws |> concat(num), min: 1))
  numline = nums |> ignore(string("\n"))

  defparsec(
    :card,
    ignore(string("Card"))
    |> concat(ws)
    |> concat(num)
    |> ignore(string(":"))
    |> concat(nums)
    |> ignore(string(" | "))
    |> concat(nums)
  )

  a_to_b_map =
    ascii_string([?a..?z], min: 1)
    |> ignore(string("-to-"))
    |> ascii_string([?a..?z], min: 1)
    |> ignore(string(" map:\n"))
    |> wrap(repeat(numline))

  defparsec(
    :seedmap,
    ignore(string("seeds:"))
    |> concat(numline)
    |> repeat(wrap(ignore(string("\n")) |> concat(a_to_b_map)))
  )
end
