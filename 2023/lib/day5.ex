defmodule RangeMap do
  def translate([dest, source, _], id) when is_number(dest) and is_number(source),
    do: dest + (id - source)

  def translate(ranges, id) do
    case Enum.find(ranges, fn [dest, source, len] -> id >= source and id < source + len end) do
      nil -> id
      rangemap -> translate(rangemap, id)
    end
  end

  def translate_range([], range), do: [range]

  def translate_range(maps, range) do
    [rangemap | rest] = maps
    [_, source, len] = rangemap
    source_end = source + len - 1
    first..last = range

    if source_end >= first and source <= last do
      case [max(source, first), min(source_end, last)] do
        [^first, ^last] ->
          [
            translate(rangemap, first)..translate(rangemap, last)
          ]

        [^first, split] ->
          [
            translate(rangemap, first)..translate(rangemap, split)
            | translate_range(rest, (split + 1)..last)
          ]

        [split, ^last] ->
          [
            translate(rangemap, split)..translate(rangemap, last)
            | translate_range(rest, first..(split - 1))
          ]

        [split_start, split_end] ->
          [
            translate(rangemap, split_start)..translate(rangemap, split_end)
            | translate_range(rest, first..(split_start - 1)) ++
                translate_range(rest, (split_end + 1)..last)
          ]
      end
    else
      translate_range(rest, range)
    end
  end
end

defmodule SeedMap do
  def new(maps) do
    maps
    |> Enum.map(fn [from, to, ranges] ->
      {String.to_atom(from), {String.to_atom(to), ranges}}
    end)
  end

  def translate(maps, {type, id}) do
    case Keyword.get(maps, type) do
      {new_type, ranges} -> translate(maps, {new_type, RangeMap.translate(ranges, id)})
      nil -> {type, id}
    end
  end

  def translate_range(maps, {type, range}) do
    case Keyword.get(maps, type) do
      {new_type, rangemaps} ->
        RangeMap.translate_range(rangemaps, range)
        |> Enum.flat_map(fn range -> translate_range(maps, {new_type, range}) end)

      nil ->
        [{type, range}]
    end
  end
end

defmodule Day5 do
  def part1(input) do
    {:ok, [seeds | maps], _, _, _, _} = Util.Parser.seedmap(input)
    map = SeedMap.new(maps)

    seeds
    |> Enum.map(fn seed -> SeedMap.translate(map, {:seed, seed}) |> elem(1) end)
    |> Enum.min()
  end

  def part2(input) do
    {:ok, [seeds | maps], _, _, _, _} = Util.Parser.seedmap(input)
    map = SeedMap.new(maps)

    seeds
    |> Enum.chunk_every(2)
    |> Enum.flat_map(fn [start, len] ->
      SeedMap.translate_range(map, {:seed, start..(start + len - 1)})
    end)
    |> Enum.min()
    |> elem(1)
    |> Enum.min()
  end
end
