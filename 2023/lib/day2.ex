defprotocol Analysis do
  def drew_at_most?(data, max_counts)
  def max_drew(data)
  def max_drew(data, init)
end

defimpl Analysis, for: List do
  def drew_at_most?(data, max_counts) do
    Enum.all?(data, &Analysis.drew_at_most?(&1, max_counts))
  end

  def max_drew(data, init \\ nil) do
    Enum.reduce(data, init, &Analysis.max_drew/2)
  end
end

defmodule Color do
  defstruct [:num, :color]

  def parse(spec) do
    [num, color] = String.split(spec)
    %__MODULE__{num: String.to_integer(num), color: String.to_existing_atom(color)}
  end

  defimpl Analysis do
    def drew_at_most?(color, max_counts) do
      color.num <= max_counts[color.color]
    end

    def max_drew(count, cur_max \\ nil) do
      %{color: color, num: num} = count

      max_map =
        case cur_max do
          m when is_map(m) -> m
          nil -> %{}
        end

      prev_max = Map.get(max_map, color, 0)

      case(num) do
        n when n > prev_max -> Map.put(max_map, color, num)
        _ -> cur_max
      end
    end
  end
end

defmodule Draw do
  def parse(spec) do
    spec |> Util.parse(Color, split: ",")
  end
end

defmodule Game do
  defstruct draws: [], num: 0

  defimpl Analysis do
    def drew_at_most?(game, max_counts) do
      game.draws |> Analysis.drew_at_most?(max_counts)
    end

    def max_drew(game, init \\ nil) do
      Analysis.max_drew(game.draws, init)
    end
  end

  def parse(spec) do
    ["Game " <> num, game_spec] = String.split(spec, ": ")

    %__MODULE__{
      draws: game_spec |> Util.parse(Draw, split: ";"),
      num: String.to_integer(num)
    }
  end

  def power(game) do
    max = Analysis.max_drew(game)
    max[:red] * max[:blue] * max[:green]
  end
end

defmodule Day2 do
  @moduledoc false

  def part1(input, counts) do
    input
    |> Util.parse(Game, split: "\n")
    |> Enum.filter(&Analysis.drew_at_most?(&1, counts))
    |> Enum.map(& &1.num)
    |> Enum.sum()
  end

  def part2(input) do
    input
    |> Util.parse(Game, split: "\n")
    |> Enum.map(&Game.power/1)
    |> Enum.sum()
  end
end
