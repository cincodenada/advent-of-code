defmodule Day1 do
  @moduledoc false

  def get_calibration(numbers) do
    # TODO: This is v ineffiencent for cons cells
    Integer.undigits([Enum.at(numbers, 0), Enum.at(numbers, -1)])
  end

  def part1(input) do
    input
    |> String.split()
    |> Enum.map(fn s -> s |> Util.get_numbers() |> get_calibration end)
    |> Enum.sum()
  end

  def part2(input) do
    input
    |> String.split()
    |> Enum.map(fn s -> s |> Util.get_spelled_numbers() |> get_calibration end)
    |> Enum.sum()
  end
end
