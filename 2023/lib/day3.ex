defmodule Schematic do
  def parse(spec) do
    %{toks: toks} = consume(spec)

    %{partnum: nums, part: parts} =
      toks |> Enum.group_by(& &1.type)

    num_map = map_nums(nums)

    parts |> Enum.map(fn p -> Map.put(p, :num, get_num(num_map, p.x, p.y)) end)
  end

  def map_nums(nums) do
    nums
    |> Enum.flat_map(fn tok ->
      for offset <- 0..(byte_size(tok.tok) - 1),
          do: {{tok.y, tok.x - offset}, String.to_integer(tok.tok)}
    end)
    |> Map.new()
  end

  def get_num(num_map, x, y) do
    for xoff <- -1..1, yoff <- -1..1, !(xoff == yoff == 0) do
      Map.get(num_map, {y + yoff, x + xoff})
    end
    |> Enum.filter(& &1)
    |> Enum.uniq()
  end

  def consume(spec, toks \\ [], pos \\ %{x: 0, y: 0}, num \\ "") do
    <<char::binary-size(1), rest::binary>> = spec

    tok =
      case char do
        d when d >= "0" and d <= "9" -> {:num, d}
        s when s in [".", "\n"] -> {:space, s}
        c -> {:part, c}
      end

    toks =
      case tok do
        {:num, _} -> toks
        _ when num == "" -> toks
        _ -> [%{x: pos.x - 1, y: pos.y, type: :partnum, tok: num} | toks]
      end

    toks =
      case tok do
        {:part, c} -> [%{x: pos.x, y: pos.y, type: :part, tok: c} | toks]
        _ -> toks
      end

    if rest == "" do
      %{toks: toks, rows: pos.y + 1, cols: pos.x}
    else
      consume(
        rest,
        toks,
        case char do
          "\n" -> %{pos | x: 0, y: pos.y + 1}
          _ -> %{pos | x: pos.x + 1}
        end,
        case tok do
          {:num, d} -> num <> d
          _ -> ""
        end
      )
    end
  end
end

defmodule Day3 do
  def part1(input) do
    Schematic.parse(input) |> Enum.flat_map(& &1.num) |> Enum.sum()
  end

  def part2(input) do
    Schematic.parse(input)
    |> Enum.flat_map(
      &case &1.num do
        [a, b] -> [a * b]
        _ -> []
      end
    )
    |> Enum.sum()
  end
end
