defmodule ScratchCard do
  defstruct [:id, :winning, :have]

  def parse(spec) do
    {:ok, [id, winning, have], rest, ctx, line, col} = Util.Parser.card(spec)

    %__MODULE__{id: id, winning: winning, have: have}
  end

  def have_wins(card) do
    MapSet.intersection(
      MapSet.new(card.winning),
      MapSet.new(card.have)
    )
  end

  def num_wins(card), do: Enum.count(have_wins(card))

  def points(card) do
    case num_wins(card) do
      0 -> 0
      n -> :math.pow(2, n - 1)
    end
  end
end

defmodule CardSet do
  use Agent

  def parse(spec) do
    cardset =
      spec
      |> String.trim()
      |> String.split("\n")
      |> Enum.map(&(&1 |> ScratchCard.parse()))
  end

  def points(cards), do: cards |> Enum.map(&ScratchCard.points/1)

  def cardcards(cardset, id) do
    maxcard = Enum.count(cardset)

    if maxcard == id do
      nil
    else
      case ScratchCard.num_wins(Enum.at(cardset, id - 1)) do
        0 -> nil
        w -> min(id + 1, maxcard)..min(id + w, maxcard)
      end
    end
  end

  def card_count(cardset, id) do
    case CardSet.cardcards(cardset, id) do
      nil -> 0
      next -> cardcards_count(cardset, next)
    end
  end

  def cardcards_count(cardset, idxs) do
    subcards =
      idxs
      |> Enum.map(fn idx -> card_count(cardset, idx) end)
      |> Enum.sum()

    subcards + Enum.count(idxs)
  end
end

defmodule Day4 do
  def part1(input) do
    input
    |> CardSet.parse()
    |> CardSet.points()
    |> Enum.sum()
  end

  def part2(input) do
    cards = input |> CardSet.parse()
    CardSet.cardcards_count(cards, cards |> Enum.map(& &1.id))
  end
end
