defmodule UtilTest do
  use ExUnit.Case
  doctest Util

  test "util" do
    assert Util.get_numbers("c5") == [5]
    assert Util.get_spelled_numbers("threefour") == [3, 4]
    assert Util.get_spelled_numbers("12345") == [1, 2, 3, 4, 5]
    assert Util.get_spelled_numbers("twone") == [2, 1]
    assert Util.get_spelled_numbers("123") == [1,2,3]
  end
end

