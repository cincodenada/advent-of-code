defmodule Day4Test do
  use ExUnit.Case
  doctest Day4

  @input File.read!("day4.txt")
  @ex1 ~s(
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
)

  test "part1" do
    [line | _] = @ex1 |> String.trim() |> String.split("\n")

    card = ScratchCard.parse(line)

    assert card == %ScratchCard{
             id: 1,
             winning: [41, 48, 83, 86, 17],
             have: [83, 86, 6, 31, 17, 9, 48, 53]
           }

    assert ScratchCard.have_wins(card) == MapSet.new([17, 48, 83, 86])
    assert ScratchCard.points(card) == 8

    assert Day4.part1(@ex1) == 13
#    assert Day4.part1(@input) == 23750
  end

  test "part2" do
    cards = @ex1 |> CardSet.parse()
    assert Enum.to_list(CardSet.cardcards(cards, 1)) == [2, 3, 4, 5]

    assert Day4.part2(@ex1) == 30
#    assert Day4.part2(@input) == 13_261_850
  end
end
