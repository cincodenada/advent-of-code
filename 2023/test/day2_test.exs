defmodule Day2Test do
  use ExUnit.Case
  doctest Day2

  @input File.read!("day2.txt")
  @ex1 ~s(
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
)

  test "part1" do
    max_counts = %{red: 12, green: 13, blue: 14}
    assert Day2.part1(@ex1, max_counts) == 8
    assert Day2.part1(@input, max_counts) == 2600
  end

  test "part2" do
    assert Day2.part2(@ex1) == 2286
    assert Day2.part2(@input) == 86036
  end
end
