defmodule Day3Test do
  use ExUnit.Case
  doctest Day3

  @input File.read!("day3.txt")
  @ex1 ~s(467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
)

  test "part1" do
    parsed = Schematic.consume(@ex1)

    assert parsed == %{
             toks: [
               %{type: :partnum, tok: "598", y: 9, x: 7},
               %{type: :partnum, tok: "664", y: 9, x: 3},
               %{type: :part, tok: "*", y: 8, x: 5},
               %{type: :part, tok: "$", y: 8, x: 3},
               %{type: :partnum, tok: "755", y: 7, x: 8},
               %{type: :partnum, tok: "592", y: 6, x: 4},
               %{type: :partnum, tok: "58", y: 5, x: 8},
               %{type: :part, tok: "+", y: 5, x: 5},
               %{type: :part, tok: "*", y: 4, x: 3},
               %{type: :partnum, tok: "617", y: 4, x: 2},
               %{type: :part, tok: "#", y: 3, x: 6},
               %{type: :partnum, tok: "633", y: 2, x: 8},
               %{type: :partnum, tok: "35", y: 2, x: 3},
               %{type: :part, tok: "*", y: 1, x: 3},
               %{type: :partnum, tok: "114", y: 0, x: 7},
               %{type: :partnum, tok: "467", y: 0, x: 2}
             ],
             rows: 10,
             cols: 10
           }

    num_map = Schematic.map_nums(parsed.toks |> Enum.filter(&(&1.type == :partnum)))

    assert num_map == %{
             {0, 0} => 467,
             {0, 1} => 467,
             {0, 2} => 467,
             {0, 5} => 114,
             {0, 6} => 114,
             {0, 7} => 114,
             {2, 2} => 35,
             {2, 3} => 35,
             {2, 6} => 633,
             {2, 7} => 633,
             {2, 8} => 633,
             {4, 0} => 617,
             {4, 1} => 617,
             {4, 2} => 617,
             {5, 7} => 58,
             {5, 8} => 58,
             {6, 2} => 592,
             {6, 3} => 592,
             {6, 4} => 592,
             {7, 6} => 755,
             {7, 7} => 755,
             {7, 8} => 755,
             {9, 1} => 664,
             {9, 2} => 664,
             {9, 3} => 664,
             {9, 5} => 598,
             {9, 6} => 598,
             {9, 7} => 598
           }

    assert Schematic.parse(@ex1) == [
             %{type: :part, tok: "*", y: 8, x: 5, num: [598, 755]},
             %{type: :part, tok: "$", y: 8, x: 3, num: [664]},
             %{type: :part, tok: "+", y: 5, x: 5, num: [592]},
             %{type: :part, tok: "*", y: 4, x: 3, num: [617]},
             %{type: :part, tok: "#", y: 3, x: 6, num: [633]},
             %{type: :part, tok: "*", y: 1, x: 3, num: [467, 35]}
           ]

    assert Day3.part1(@ex1) == 4361
    assert Day3.part1(@input) == 526404
  end

  test "part2" do
   assert Day3.part2(@ex1) == 467835
   assert Day3.part2(@input) == 84399773
  end
end
