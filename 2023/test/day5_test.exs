defmodule Day5Test do
  use ExUnit.Case
  import Util.Macros
  doctest Day5

  @input File.read!("day5.txt")
  @ex1 ~s(seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
)

  test "parse" do
    {:ok, parsed, _, _, _, _} = Util.Parser.seedmap(@ex1)

    assert Enum.slice(parsed, 0, 3) ==
             [
               [79, 14, 55, 13],
               ["seed", "soil", [[50, 98, 2], [52, 50, 48]]],
               [
                 "soil",
                 "fertilizer",
                 [
                   [0, 15, 37],
                   [37, 52, 2],
                   [39, 0, 15]
                 ]
               ]
             ]
  end

  test "part1" do
    exmap =
      [
        [0, 15, 37],
        [37, 52, 2],
        [39, 0, 15]
      ]

    assert RangeMap.translate(exmap, 5) == 39 + 5
    assert RangeMap.translate(exmap, 20) == 5
    assert RangeMap.translate(exmap, 150) == 150

    {:ok, [seeds | maps], _, _, _, _} = Util.Parser.seedmap(@ex1)
    map = SeedMap.new(maps)

    assert SeedMap.translate(map, {:seed, 79}) == {:location, 82}

    assert Day5.part1(@ex1) == 35
    assert Day5.part1(@input) == 600_279_879
  end

  test "part2" do
    exmap =
      [
        [0, 15, 37],
        [37, 52, 2],
        [39, 0, 15]
      ]

    assert RangeMap.translate_range(exmap, 0..20) == [
             0..5,
             39..(39 + 14)
           ]

    assert RangeMap.translate_range(exmap, 150..200) == [
             150..200
           ]

    simple_map = [[100, 0, 10], [200, 10, 10], [300, 20, 10]]

    assert RangeMap.translate_range(simple_map, 0..30) == [
             100..109,
             200..209,
             300..309,
             30..30
           ]

    assert RangeMap.translate_range(simple_map, 5..25) == [
             105..109,
             200..209,
             300..305
           ]

    assert RangeMap.translate_range(simple_map, -10..15) == [
             100..109,
             -10..-1,
             200..205
           ]

    run d5p2 do
      @ex1 -> 46
      @input -> 20_191_102
    end
  end
end
