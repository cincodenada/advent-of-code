defmodule Day1Test do
  use ExUnit.Case
  doctest Day1

  @input File.read!("day1.txt")
  @ex1 "1abc2
    pqr3stu8vwx
    a1b2c3d4e5f
    treb7uchet"
  @ex2 "two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"

  test "part1" do
    assert Day1.part1(@ex1) == 142
    assert Day1.part1(@input) == 54644
  end

  test "part2" do
    assert Day1.part2(@ex2) == 281
    assert Day1.part2(@input) == 53348
  end
end
